package com.ufrn.guiasocial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ufrn.adapter.PrintTweetsAdapter;
import com.ufrn.guiasocial.R;
import com.ufrn.layoutstab.ActivityTabs;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
	//Codigo Feito Por Mickael e Jos� Lucas
@SuppressLint("NewApi")
public class TwitterActivity extends Activity {


 
    int Lingua;
    
    
	private ListView listView;
	ArrayList<String> itens = new ArrayList<String>();
	private ProgressBar bar;
	int cont = 0;
	int result;
	

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.timeline);
		//configurando o action bar
        ActionBar actionBar = getActionBar();
		 actionBar.setDisplayUseLogoEnabled(false);
		 actionBar.setDisplayShowTitleEnabled(false);
		 actionBar.setDisplayHomeAsUpEnabled(true);
		ProgressDialog progressDialog = ProgressDialog.show(TwitterActivity.this,"Loading...",  
			    "O FindNatal est� carregando os Tweets, aguarde um momento...", true, true);  
		listView = (ListView) findViewById(R.id.lista);
		
		 Intent it = getIntent();
	     Lingua =it.getIntExtra("Lingua", Lingua);
		
		insertT();
		
	}
	
	//Armazenando Os Tweets
	public void insertT() {	
		//Configurando permissoes e ligando stream.
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		cb.setOAuthConsumerKey("UwtdvfOqwSyH8rOwsXDWA");
		cb.setOAuthConsumerSecret("DdCThaZj6S8Y1SmCRNvKcQKyBuFNcKQzq8gq6Ls2Q");
		cb.setOAuthAccessToken("295370953-9JWb4ntgPQYQtm5s37xA6R5mtiBJtrHKTemqwDPL");
		cb.setOAuthAccessTokenSecret("YigMYXvc06US0uNpRlkLeu0kTCUPB9YbRBcNWsEBYRjBF");
		final TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

		StatusListener listener = new StatusListener() {
			//Adicionando os Twittes Sob Efeito do Filtro A o Array;
			
			public void onStatus(Status status) {
				if(itens.size()<10){
				System.out.println("@" + status.getUser().getScreenName()+ " - " + status.getText());
				result=filtro(status.getText().toString());
				if(result==1){
					String tt1 =status.getText().toString();
					itens.add(tt1);
					System.out.println(itens.size());
					bar.setProgress(itens.size());
				}
				}
				else{
				twitterStream.shutdown();
				Intent i = new Intent(TwitterActivity.this, PrintTweetsAdapter.class);   
				i.putStringArrayListExtra("Tweets", itens);
				startActivity(i);
				}
			}

			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {	
			}

			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
			}

			public void onScrubGeo(long userId, long upToStatusId) {
			}

			public void onException(Exception ex) {
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
			}

		};

		FilterQuery fq = new FilterQuery();
		String keywords[] = {"Brasil" };//"Rio Grande do Norte","Ribeira","Potiguares","Ponta Negra","Via Costeira","Forte dos Reis Magos","Arena das Dunas","Cultura POtiguar","Forro em Natal","Praia de Pipa"};
		//double[][] location = { { -74, 40 }, { -73, 41 } };
		String lingua[] = { "pt" };
		fq.track(keywords).language(lingua);
		twitterStream.addListener(listener);
		twitterStream.filter(fq);
		

	}
	//Abrindo tweets no android!
	public void chamar() {
		 final StableArrayAdapter adapter = new StableArrayAdapter(this,android.R.layout.simple_list_item_1, itens);
		 listView.setAdapter(adapter);

	}
	
	public class StableArrayAdapter extends ArrayAdapter<String> {

		    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		    public StableArrayAdapter(Context context, int textViewResourceId,
		        List<String> objects) {
		      super(context, textViewResourceId, objects);
		      for (int i = 0; i < objects.size(); ++i) {
		        mIdMap.put(objects.get(i), i);
		      }
		    }

		    @Override
		    public long getItemId(int position) {
		      String item = getItem(position);
		      return mIdMap.get(item);
		    }

		    @Override
		    public boolean hasStableIds() {
		      return true;
		    }

		  }

	//Filtro Criado por Jos� Lucas->
	
	public static int filtro(String frase){
		frase=frase.toLowerCase();
		frase=frase+" ";
		int asc;
		int tamanho=frase.length();
		int cont=0;
		asc=(int)frase.charAt(cont);
		String palavra="";
		
		while(tamanho>cont){
			while(asc!=32){
				if((asc>=65 && asc<=90) || (asc>=97 && asc<=122)){
				palavra=palavra+frase.charAt(cont);
				}
				cont++;
				asc=(int)frase.charAt(cont);
			}
			if(asc==32){
				if(palavra.equals("porra") || palavra.equals("krl") || palavra.equals("caralho") || palavra.equals("buceta") || palavra.equals("puta")|| palavra.equals("poha")|| palavra.equals("foda")|| palavra.equals("n�o vai ter")|| palavra.equals("#n�ovaitercopa")|| palavra.equals("merda")|| palavra.equals("lixo")){
					return 0;
				}
				else{
					palavra="";
					cont++;
					if(cont<tamanho){
					asc=(int)frase.charAt(cont);
					}
				}
			}
			
		}
		
		return 1;
		}
	
	
	public void twittar(View v){
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Estou Usando o FindNatal e Estou Adorando!! Recomendo");
        startActivity(Intent.createChooser(shareIntent, "Condividi con...")); 
        
	}
	
	public void menu(View v){
		this.finish();
	}
	
	@Override

	/*
	public boolean onMenuItemSelected(int panel, MenuItem item){
		switch(item.getItemId()){
			case android.R.id.home:
			this.finish();	
			break;				
		}	
		
		return(true);
	}
	*/
	
	public void onPause() {
	    super.onPause();
	    this.finish();
	}
	
	public void onStop() {
	    super.onStop();
	    this.finish();
	}
	
	public void onDestroy(){
		super.onDestroy();
		this.finish();
	}
	
	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click
					if(item.getItemId()==android.R.id.home){ 
						this.finish();
					}
	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    		return true;
	    	}
	    
	
}

package com.ufrn.guiasocial;

import com.ufrn.layoutstab.ActivityTabs;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ServicesActivity extends Activity {

	int Lingua;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab4_view);
		//configurando o action bar
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
    	
    	if(Lingua==0){
    	getMenuInflater().inflate(R.menu.main, menu);
	    }else{
	    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
	    }
	    return true;
		
    }
	
	public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click

	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){

	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    		return true;
	    	}
	    
	
}

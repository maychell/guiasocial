package com.ufrn.guiasocial;

import java.util.List;

import com.ufrn.adapter.SubinformationAdapter;
import com.ufrn.dao.DaoSubinformation;
import com.ufrn.domain.Subinformation;
import com.ufrn.layoutstab.ActivityTabs;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class SubinformationActivity extends ListActivity {

	private TextView txtSubinformationName;
	private DaoSubinformation dao;
	private List<Subinformation> subinformation;
	private long idSubtypeInformation;
    int Lingua;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		dao = new DaoSubinformation(this);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		
		Intent it = getIntent();
		Lingua =it.getIntExtra("Lingua", Lingua);
		
		if(it != null){
			//Bundle params = it.getExtras();
			if(it.getIntExtra("botao", 0) != 0){
				idSubtypeInformation = it.getIntExtra("botao",0);
				Log.d ("BOTAOOOOOOOOOOOOOO", "Cliquei em:" + idSubtypeInformation);
			}
		}
		
		switch (it.getIntExtra("botao",0)) {
		case 1:
			setContentView(R.layout.tab1_view);	
			break;
		case 2:
			setContentView(R.layout.tab2_view);
			break;
		case 3:
			setContentView(R.layout.tab3_view);
			break;
		case 4:
			setContentView(R.layout.tab4_view);
			break;
		default:
			break;
		}
		txtSubinformationName = (TextView) findViewById(R.id.tvInformationName);
		
		list();
		
	}
	
	private void list() {
		subinformation = dao.getAllSubtipoInformacao(idSubtypeInformation);
		if(subinformation != null && subinformation.size() > 0)
			this.setListAdapter(new SubinformationAdapter(this, subinformation));
		else
			txtSubinformationName.setText("Nenhum registro encontrado");
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Subinformation t = subinformation.get(position);
		Intent i = new Intent(this, TouristInformationActivity.class);
		i.putExtra("id_tourist_information", t.getId());
		startActivity(i);
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.main, menu);
	    return true;
	}
	
	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click

	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    		return true;
	    	}
	    
}

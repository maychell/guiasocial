package com.ufrn.guiasocial;

import java.util.List;

import com.ufrn.adapter.SearchAdapter;
import com.ufrn.adapter.TouristInformationAdapter;
import com.ufrn.dao.DaoSearchInformation;
import com.ufrn.dao.DaoTouristinformation;
import com.ufrn.domain.Touristinformation;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class SearchActivity extends ListActivity {
	private DaoSearchInformation dao2;
	private List<Touristinformation> list;
	private String search;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tourist_information);
	
		// enable the home button
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayUseLogoEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(true); 
		
		//txt_tourist_name_value = (TextView) findViewById(R.id.tvInformationName);
		//txt_tourist_description_value = (TextView) findViewById(R.id.txt_tourist_information_description_value);
		
		dao2 = new DaoSearchInformation(this);
		
		Intent it = getIntent();
		search= it.getStringExtra("data");
		list();
		
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home:
	      Intent intent = new Intent(this, MainActivity.class);
	      //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	      startActivity(intent);
	      break;
	    
	    default:
	      break;
	    }
	    return super.onOptionsItemSelected(item);
	  }
	
	
	private void list() {
		list = dao2.getAllSearchInformation(search);
		if(list != null && list.size() > 0)
			this.setListAdapter(new SearchAdapter(this, list));
		else
			Toast.makeText(this, "Nenhum registro encontrado" , Toast.LENGTH_SHORT).show();
		    
			//txt_tourist_name_value.setText("Nenhum registro encontrado");
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Touristinformation t = list.get(position);
		Intent i = new Intent(this, TouristInformationDetailsActivity.class);
		i.putExtra("id_tourist_info", t.getId());
		startActivity(i); 	
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao2 != null)
			dao2.close();
	}

	protected void onPause(){
		super.onPause();
		this.finish();
	}
	
}

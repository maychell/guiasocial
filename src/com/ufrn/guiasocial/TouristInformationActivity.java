package com.ufrn.guiasocial;

import java.util.List;

import com.ufrn.adapter.Subinformation_detailsAdapter;
import com.ufrn.adapter.TouristInformationAdapter;
import com.ufrn.dao.DaoSubinformation_details;
import com.ufrn.dao.DaoTouristinformation;
import com.ufrn.domain.Subinformation_details;
import com.ufrn.domain.Touristinformation;
import com.ufrn.layoutstab.ActivityTabs;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TouristInformationActivity extends ListActivity{
	
	//private TextView txt_tourist_name_value;
	//private TextView txt_tourist_description_value;
	private int Lingua;
	private DaoTouristinformation dao;
	private List<Touristinformation> touristinformation;
	private long idTouristInformation;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tourist_information);
	
		// enable the home button
		 ActionBar actionBar = getActionBar();
		 actionBar.setDisplayUseLogoEnabled(false);
		 actionBar.setDisplayShowTitleEnabled(false);
		 actionBar.setDisplayHomeAsUpEnabled(true); 
		
		
		//txt_tourist_name_value = (TextView) findViewById(R.id.tvInformationName);
		//txt_tourist_description_value = (TextView) findViewById(R.id.txt_tourist_information_description_value);
		
		dao = new DaoTouristinformation(this);
		
		Intent it = getIntent();
		if(it != null){
			//Bundle params = it.getExtras();
			if(it.getLongExtra("id_tourist_information", 0) != 0){
				idTouristInformation = it.getLongExtra("id_tourist_information", 0);
				Log.d ("ID SUBTIPO INFORMATION", "Cliquei em:" + idTouristInformation);
			}
		}
		
		list();
		
	}
		
	private void list() {
		touristinformation = dao.getAllTipoInformacao(idTouristInformation);
		if(touristinformation != null && touristinformation.size() > 0)
			this.setListAdapter(new TouristInformationAdapter(this, touristinformation));
		else
			Toast.makeText(this, "Nenhum registro encontrado" , Toast.LENGTH_SHORT).show();
		    
			//txt_tourist_name_value.setText("Nenhum registro encontrado");
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Touristinformation t = touristinformation.get(position);
		Intent i = new Intent(this, TouristInformationDetailsActivity.class);
		i.putExtra("id_tourist_info", t.getId());
		startActivity(i); 	
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
	
	 public boolean onCreateOptionsMenu(Menu menu) {
	    	
	    	Intent it = getIntent();
	        Lingua =it.getIntExtra("Lingua", Lingua);
	    	
	    	if(Lingua==0){
	    	getMenuInflater().inflate(R.menu.main, menu);
		    }else{
		    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
		    }
		    return true;
	    }
	 
	 public boolean onOptionsItemSelected(MenuItem item) {
			if(item.getItemId()==android.R.id.home){ 
				this.finish();
			}
			if(item.getItemId()==R.id.twitter_eng){
				Intent it = new Intent(this, TwitterActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it); 
			}
			if(item.getItemId()==R.id.services_eng){
				Intent it = new Intent(this, ServicesActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.meet_natal_eng){
				Intent it = new Intent(this, ActivityTabs.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.maps_eng){
				Intent it = new Intent(this, MapsActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.twitter){
				Intent it = new Intent(this, TwitterActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it); 
			}
			if(item.getItemId()==R.id.services){
				Intent it = new Intent(this, ServicesActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
				
			}
			if(item.getItemId()==R.id.meet_natal){
				Intent it = new Intent(this, ActivityTabs.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.maps){
				Intent it = new Intent(this, MapsActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.routes){
				Intent it = new Intent(this, RoutesActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
			if(item.getItemId()==R.id.routes_eng){
				Intent it = new Intent(this, RoutesActivity.class);  
				it.putExtra("Lingua", Lingua);
				startActivity(it);
			}
		return true;
		  }
}

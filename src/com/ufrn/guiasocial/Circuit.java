package com.ufrn.guiasocial;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class Circuit {
	  private final List<LatLng> points;  
	  private String polyline;  
	  
	  public Circuit() {  
	    points = new ArrayList<LatLng>();  
	  }  
	  
	  public void addPoints(final List<LatLng> points) {  
	    this.points.addAll(points);  
	  }  
	  
	  public List<LatLng> getPoints() {  
	    return points;  
	  }  
	  
	  public void setPolyline(String polyline) {  
	    this.polyline = polyline;  
	  }  
	  
	  public String getPolyline() {  
	    return polyline;  
	  }  
	}  
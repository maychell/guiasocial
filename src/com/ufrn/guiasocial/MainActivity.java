package com.ufrn.guiasocial;

import com.ufrn.database.DatabaseScript;
import com.ufrn.database.Database;
import com.ufrn.guiasocial.MainActivity;
import com.ufrn.layoutstab.ActivityTabs;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ufrn.service.LocationService;

public class MainActivity extends Activity implements OnClickListener {
	int Lingua=0,tab;
	private static Database dao;
	private TextView data;
	private LinearLayout imgTwitter, imgMaps, meetNatal,imgFood,imgHotels,imgServices;
	private RelativeLayout imgBandeira;
	private ImageView imgPesq;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_eng);
		
		dao = new DatabaseScript(this);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		data = (TextView) findViewById(R.id.auto_complete_searsh);
		imgTwitter = (LinearLayout) findViewById(R.id.menu_twitter);
		imgMaps = (LinearLayout) findViewById(R.id.menu_maps);
		meetNatal = (LinearLayout) findViewById(R.id.menu_meet_natal);
		imgFood =(LinearLayout) findViewById(R.id.menu_food);
		imgHotels =(LinearLayout) findViewById(R.id.menu_hotels);
		imgBandeira=(RelativeLayout) findViewById(R.id.menu_bandeira);
		imgServices=(LinearLayout) findViewById(R.id.menu_services);
		imgBandeira=(RelativeLayout) findViewById(R.id.menu_bandeira);
		imgPesq=(ImageView) findViewById(R.id.img_search);
		registerListeners();
        
		meetNatal.setOnClickListener(this);
		imgServices.setOnClickListener(this);
		imgPesq.setOnClickListener(this);
		//Altera��o Ademir para verificar a conex�o com a internet

        registerListeners();

		if (TemConexao()){
            trace("tem conexao.");
        }else{
            trace("sem conexao.");
        }
		
		imgHotels.setOnClickListener(this);
		imgFood.setOnClickListener(this);
		imgTwitter.setOnClickListener(this);
		imgMaps.setOnClickListener(this);
		imgBandeira.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {		
		if(v.getId() == R.id.img_search) {	 
			Intent it = new Intent(this, SearchActivity.class);
			it.putExtra("data", data.getText().toString());
			System.out.println("YAGYGSYAGSYAGSYG"+data);
			startActivity(it);
		}
		if(v.getId() == R.id.menu_bandeira) {	
			Intent it = new Intent(this, MainActivity2.class);  
			startActivity(it);  
			this.finish();
		}
		if(v.getId() == R.id.menu_twitter) {
			Intent it = new Intent(this, TwitterActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);  
		}
	
		if(v.getId() == R.id.menu_maps) {
			Intent it = new Intent(this, MapsActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(v.getId() == R.id.menu_meet_natal){
			Intent it = new Intent(this, ActivityTabs.class);
			it.putExtra("botao",1);
			tab=0;
			it.putExtra("tab", tab);
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(v.getId() == R.id.menu_food){
			Intent it = new Intent(this, ActivityTabs.class);
			tab=1;
			it.putExtra("botao",2);
			it.putExtra("tab", tab);
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(v.getId() == R.id.menu_hotels){
			Intent it = new Intent(this, ActivityTabs.class);
			tab=2;
			it.putExtra("botao",3);
			it.putExtra("tab", tab);
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(v.getId() == R.id.menu_services){
			Intent it = new Intent(this, ServicesActivity.class);
			it.putExtra("botao",4);
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
	}
	
	private void registerListeners() {
		startService(new Intent(this, LocationService.class));
	}
	
	private boolean TemConexao() {
		boolean lblnRet = false;
        try
        {
            ConnectivityManager cm = (ConnectivityManager)
            getSystemService(Context.CONNECTIVITY_SERVICE); 
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) { 
                lblnRet = true; 
            } else { 
                lblnRet = false; 
            }
        }catch (Exception e) {
            trace(e.getMessage());
        }
        return lblnRet;
    }
	 
    public void toast (String msg){
        Toast.makeText (getApplicationContext(), msg, Toast.LENGTH_SHORT).show ();
    } 
  
    private void trace (String msg){
        Log.d ("teste", msg);
        toast (msg);
    } 

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
}

package com.ufrn.guiasocial;

import java.util.List;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.ufrn.adapter.RouteAdapter;
import com.ufrn.dao.DaoRoute;
import com.ufrn.domain.Route;
import com.ufrn.layoutstab.ActivityTabs;

public class RoutesActivity extends ListActivity {

	private DaoRoute dao;
	private List<Route> info;
	private int Lingua;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_routes);
        ActionBar actionBar = getActionBar();
		 actionBar.setDisplayUseLogoEnabled(false);
		 actionBar.setDisplayShowTitleEnabled(false);
		 actionBar.setDisplayHomeAsUpEnabled(true); 
		Intent it = getIntent();
	    Lingua =it.getIntExtra("Lingua", Lingua);
		dao = new DaoRoute(this);
		
		list();
	}
	
	private void list() {
		info = dao.getAllRoutes();
		if(info != null && !info.isEmpty())
			this.setListAdapter(new RouteAdapter(this, info));
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
	
		Route t = info.get(position);
		
		Intent i = new Intent(this, MapsActivity.class);
		Bundle params = new Bundle();
		params.putLong("id_date", t.getId());
		i.putExtras(params);
		this.finish();
		startActivity(i);
	}
	
    public boolean onCreateOptionsMenu(Menu menu) {
		Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
    	
    	if(Lingua==0){
    	getMenuInflater().inflate(R.menu.main, menu);
	    }else{
	    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
	    }
	    return true;
		
    }
	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click
		   			if(item.getItemId()==android.R.id.home){ 
						this.finish();
					}
	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				this.finish();
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				this.finish();
	    				startActivity(it);
	    			}
	    		return true;
	    	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
	
}

package com.ufrn.guiasocial;

import java.util.List;

import com.ufrn.adapter.InformationAdapter;
import com.ufrn.dao.DaoInformation;
import com.ufrn.domain.Information;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class InformationActivity extends ListActivity {

	private TextView txtInformationName;
	private DaoInformation dao;
	private List<Information> info;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_informations);
		
		txtInformationName = (TextView) findViewById(R.id.tvInformationName);
		dao = new DaoInformation(this);
		
		list();
		
	}
	
	private void list() {
		info = dao.getAllTipoInformacao();
		if(info != null && info.size() > 0)
			this.setListAdapter(new InformationAdapter(this, info));
		else
			txtInformationName.setText("Nenhum registro encontrado");
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Information t = info.get(position);
		
		Intent i = new Intent(this, SubinformationActivity.class);
		Bundle params = new Bundle();
		params.putLong("id_subtype_information", t.getId());
		i.putExtras(params);
		startActivity(i);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
}

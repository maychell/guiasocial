package com.ufrn.guiasocial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ufrn.dao.DaoRoute;
import com.ufrn.dao.DaoRouteDetails;
import com.ufrn.domain.Route;
import com.ufrn.domain.RouteDetails;
import com.ufrn.layoutstab.ActivityTabs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

@SuppressWarnings("unused")
public class MapsActivity extends FragmentActivity implements LocationListener{
 
	int Lingua;
	
    GoogleMap mGoogleMap;
    //Spinner spnRoutes;
    Spinner mSprPlaceType;
    private DaoRoute dao;
	private List<Route> routes;
	ArrayList<LatLng> markerPoints;
    String[] mPlaceType=null;
    String[] mPlaceTypeName=null;
    LatLng Dest = null;
    double mLatitude=-5.821608;
    double mLongitude=-35.208635;
    private AlertDialog dialog;
    
    private DaoRouteDetails routeStorage;
	private Long idDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        routeStorage = new DaoRouteDetails(this);   
        routeStorage.dump();
        // Getting reference to the Spinner
       // spnRoutes = (Spinner) findViewById(R.id.spr_place_type);
        // Array of place types
        mPlaceType = getResources().getStringArray(R.array.place_type);
         // Array of place type names
        mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);
         // Creating an array adapter with an array of Place types
        // to populate the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, mPlaceTypeName);
         // Getting reference to the Spinner
        mSprPlaceType = (Spinner) findViewById(R.id.spr_place_type);
         // Setting adapter on Spinner to set place types
        mSprPlaceType.setAdapter(adapter);
        Button btnFind;
        btnFind = ( Button ) findViewById(R.id.btn_find);
        btnFind.setOnClickListener(new OnClickListener() {
        	 
            @Override
            public void onClick(View v) {

                int selectedPosition = mSprPlaceType.getSelectedItemPosition();
                String type = mPlaceType[selectedPosition];

                StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                sb.append("location="+mLatitude+","+mLongitude);
                sb.append("&rankby=distance");
                sb.append("&types="+type);
                sb.append("&sensor=true");
                sb.append("&key=AIzaSyAXg7zHkzHDnFR3flUB6Saxi_tlmnSgHKY");

                // Creating a new non-ui thread task to download json data
                PlacesTask placesTask = new PlacesTask();

                // Invokes the "doInBackground()" method of the class PlaceTask
                placesTask.execute(sb.toString());

            }
        });
        
         ActionBar actionBar = getActionBar();
		 actionBar.setDisplayUseLogoEnabled(false);
		 actionBar.setDisplayShowTitleEnabled(false);
		 actionBar.setDisplayHomeAsUpEnabled(true); 
        
        try {
        	dao = new DaoRoute(this);
 //       	spinnerAdapter();
        } catch(Exception e) {
        	e.printStackTrace();
        }
 
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
 
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
 
            int requestCode = 10;
            Dialog dialogg = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialogg.show();
 
        }else { // Google Play Services are available
 
            // Getting reference to the SupportMapFragment
            SupportMapFragment fragment = ( SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
 
            // Getting Google Map

            mGoogleMap = fragment.getMap();
            
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
 
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
            if (provider == null){
            	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
        				MapsActivity.this);
         
        			// set title
        			alertDialogBuilder.setTitle("Aviso");
         
        			// set dialog message
        			alertDialogBuilder
        				.setMessage("Libere o acesso a sua localização nas configuração do seu dispositivo.")
        				.setCancelable(false)
        				.setPositiveButton("Configurações",new DialogInterface.OnClickListener() {
        					public void onClick(DialogInterface dialog,int id) {

        		            	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);   
        		            	startActivityForResult(intent, 1);
        		                   		   
 
        					}
        				  })
        				.setNegativeButton("Usar sem localização",new DialogInterface.OnClickListener() {
        					public void onClick(DialogInterface dialog,int id) {
        						// if this button is clicked, just close
        						// the dialog box and do nothing
        						dialog.cancel();
        					}
        				});
         
        				// create alert dialog
        				AlertDialog alertDialog = alertDialogBuilder.create();
         
        				// show it
        				alertDialog.show();
        			}
            if(provider != null){
            // Getting Current Location From GPS
            final Location location = locationManager.getLastKnownLocation(provider);
            
            mGoogleMap.setOnMapClickListener(new OnMapClickListener() {
				@Override
                public void onMapClick(LatLng point) {
     
                	// Creating a marker
                    MarkerOptions markerOptions = new MarkerOptions();
     
                    // Setting the position for the marker
                    markerOptions.position(point);
                    
                    // Setting the title for the marker.
                    // This will be displayed on taping the marker
                    markerOptions.title("Destino");
                    
                    // Clears the previously touched position
                    mGoogleMap.clear();
                    
                    // Animating to the touched position
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(point));
     
                    // Placing a marker on the touched position
                    mGoogleMap.addMarker(markerOptions);
                    if(checkConn(MapsActivity.this)==true){
                    new RotaAsyncTask(MapsActivity.this, mGoogleMap).execute(location.getLatitude(), location.getLongitude(),point.latitude,point.longitude);
                    }
                    
                }
                
            });
            
            try {
            	Intent it = getIntent();
            	if(it != null){
            		Bundle params = it.getExtras();
            		Log.d("ERRO", ""+params);
            		if(params != null){
            			idDate = params.getLong("id_date");
            		}
            	}
            	}catch (Exception e) {
            		e.printStackTrace();
            }
           
            if(idDate != null && idDate > 0)
            	routes();
            
            // Enabling MyLocation in Google Map
            mGoogleMap.setMyLocationEnabled(true);

            
            if(Dest != null){
            new RotaAsyncTask(this, mGoogleMap).execute(
            	      // Latitude, Logintude de Origem  
            	      location.getLatitude(),location.getLongitude() ,      
            	      // Latitude, Longitude de Destino  
            	      Dest.latitude,Dest.longitude);
            }
            if(location!=null){
                onLocationChanged(location);
            }
            }
            }
    }

    
   /* private void spinnerAdapter() {
    	routes = dao.getAllRoutes();
		if(routes != null && !routes.isEmpty()) {
			ArrayList<String> dates = new ArrayList<String>();
			dates.add("-------");
			for(Route r : routes)
				dates.add(r.getDate());
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, dates);
			ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
			spnRoutes.setAdapter(spinnerArrayAdapter);
			
			spnRoutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				 
				@Override
				public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
					String date = parent.getItemAtPosition(posicao).toString();
					if(!date.equals("-------"))
						idDate = dao.findRouteByDate(date).getId();
					else
						idDate = null;
				}
	 
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
	 
				}
			});
		}
    }*/
    
    private void routes() {
        PolylineOptions linha = new PolylineOptions().width(1).geodesic(true);
    	List<RouteDetails> routes = routeStorage.getRouteDetailsByRoute(idDate);                	           
		for (RouteDetails route : routes) {
			linha.color(Color.BLUE);
			linha.width(3);
			linha.add(new LatLng(route.getLatitude(), route.getLongitude()));
		}
        mGoogleMap.addPolyline(linha);
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
    	
    	if(Lingua==0){
    	getMenuInflater().inflate(R.menu.main, menu);
	    }else{
	    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
	    }
	    return true;
        }
    
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
 
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
 
            // Connecting to url
            urlConnection.connect();
 
            // Reading data from url
            iStream = urlConnection.getInputStream();
 
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
 
            StringBuffer sb  = new StringBuffer();
 
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }
 
            data = sb.toString();
 
            br.close();
 
        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
 
        return data;
    }
    
    /** A class, to download Google Places */
    private class PlacesTask extends AsyncTask<String, Integer, String>{
 
        String data = null;
 
        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }
 
        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();
 
            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
 
    }
    
 
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{
 
        JSONObject jObject;
 
        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {
 
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
 
            try{
                jObject = new JSONObject(jsonData[0]);
 
                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);
 
            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }
 
        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){
 
            // Clears all the existing markers
            mGoogleMap.clear();
 
            for(int i=0;i<list.size();i++){
 
            	//Criar um objeto do tipo marcacao e criar um DAO para este elemento
            	//Verificar no dao se existe registro com aquele identificador.
            	//Se existir, nao salvar. Se nao existir, salvar a marcacao no banco de dados.
            	
                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();
 
                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);
 
                // Getting latitude of the place
                double lat = Double.parseDouble(hmPlace.get("lat"));
 
                // Getting longitude of the place
                double lng = Double.parseDouble(hmPlace.get("lng"));
 
                // Getting name
                String name = hmPlace.get("place_name");
 
                // Getting vicinity
                String vicinity = hmPlace.get("vicinity");
 
                LatLng latLng = new LatLng(lat, lng);
 
                // Setting the position for the marker
                markerOptions.position(latLng);
 
                // Setting the title for the marker.
                //This will be displayed on taping the marker
                markerOptions.title(name + " : " + vicinity);
 
                // Placing a marker on the touched position
                mGoogleMap.addMarker(markerOptions);
            }
        }
    }
 
    @Override
    public void onLocationChanged(Location location) {
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng latLng = new LatLng(mLatitude, mLongitude);
 
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
    }
 
   @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	if(dao != null)
    		dao.close();
    }
    
	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click
		   			if(item.getItemId()==android.R.id.home){ 
						this.finish();
					}
	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				this.finish();
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				this.finish();
	    				startActivity(it);
	    			}
	    		return true;
	    	}
    
    public static boolean checkConn(Context ctx) {
        ConnectivityManager conMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
          return false;
        if (!i.isConnected())
          return false;
        if (!i.isAvailable())
          return false;
        return true;
    }
}
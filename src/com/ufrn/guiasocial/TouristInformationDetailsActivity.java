package com.ufrn.guiasocial;

import java.util.List;

import com.ufrn.adapter.Subinformation_detailsAdapter;
import com.ufrn.adapter.TouristInformationDetailsAdapter;
import com.ufrn.dao.DaoSubinformation_details;
import com.ufrn.dao.DaoTouristinformationDetails;
import com.ufrn.domain.Subinformation_details;
import com.ufrn.domain.TouristinformationDetails;
import com.ufrn.layoutstab.ActivityTabs;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class TouristInformationDetailsActivity extends Activity {

	private TextView txt_subtype_information_name_value;
	private TextView txt_subtype_information_endereco_value;
	private TextView txt_subtype_information_description_value;
	private DaoTouristinformationDetails dao;
	private List<TouristinformationDetails> information_details;
	private int Lingua;
	private long idTouristInformation;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subinformations_details);
		
		
		
		// enable the home button
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayUseLogoEnabled(false);
	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(true); 
		
		txt_subtype_information_name_value = (TextView) findViewById(R.id.txt_subtype_information_name_value);
		txt_subtype_information_description_value = (TextView) findViewById(R.id.txt_subtype_information_description_value);
		txt_subtype_information_endereco_value = (TextView) findViewById(R.id.txt_subtype_information_endereco_value);
		
		dao = new DaoTouristinformationDetails(this);
		
		Intent it = getIntent();
		if(it != null){
			//Bundle params = it.getExtras();
			if(it.getLongExtra("id_tourist_info", 0) != 0){
				idTouristInformation = it.getLongExtra("id_tourist_info", 0);
				Log.d ("ID TIPO INFORMATION_DETAIL", "Cliquei em:" + idTouristInformation);
			}
		}
		
		list();
		
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==android.R.id.home){ 
			this.finish();
		}
		if(item.getItemId()==R.id.twitter_eng){
			Intent it = new Intent(this, TwitterActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it); 
		}
		if(item.getItemId()==R.id.services_eng){
			Intent it = new Intent(this, ServicesActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.meet_natal_eng){
			Intent it = new Intent(this, ActivityTabs.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.maps_eng){
			Intent it = new Intent(this, MapsActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.twitter){
			Intent it = new Intent(this, TwitterActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it); 
		}
		if(item.getItemId()==R.id.services){
			Intent it = new Intent(this, ServicesActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
			
		}
		if(item.getItemId()==R.id.meet_natal){
			Intent it = new Intent(this, ActivityTabs.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.maps){
			Intent it = new Intent(this, MapsActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.routes){
			Intent it = new Intent(this, RoutesActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
		if(item.getItemId()==R.id.routes_eng){
			Intent it = new Intent(this, RoutesActivity.class);  
			it.putExtra("Lingua", Lingua);
			startActivity(it);
		}
	return true;
	  }
	
	private void list() {
		information_details = dao.getAllTipoInformacao(idTouristInformation);
		if(information_details != null && information_details.size() > 0){
			for (int i = 0; i < information_details.size(); i++) {
				txt_subtype_information_name_value.setText(information_details.get(i).getNome());
				txt_subtype_information_description_value.setText(information_details.get(i).getDescricao());
				txt_subtype_information_endereco_value.setText(information_details.get(i).getEndereco());	
			}
			//this.setListAdapter(new TouristInformationDetailsAdapter(this, information_details));
		}
		else
			txt_subtype_information_name_value.setText("Nenhum registro encontrado");
	}
	
	
	
	protected void onDestroy() {
		super.onDestroy();
		if(dao != null)
			dao.close();
	}
}

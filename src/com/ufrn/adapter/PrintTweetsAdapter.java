package com.ufrn.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ufrn.guiasocial.MapsActivity;
import com.ufrn.guiasocial.R;
import com.ufrn.guiasocial.RoutesActivity;
import com.ufrn.guiasocial.ServicesActivity;
import com.ufrn.guiasocial.TwitterActivity;
import com.ufrn.layoutstab.ActivityTabs;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PrintTweetsAdapter extends Activity {
		int Lingua;
		private ListView listView;
		protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.imprimir_tweets);
		listView = (ListView) findViewById(R.id.lista);
		Intent i = getIntent();
		
        ActionBar actionBar = getActionBar();
		 actionBar.setDisplayUseLogoEnabled(false);
		 actionBar.setDisplayShowTitleEnabled(false);
		 actionBar.setDisplayHomeAsUpEnabled(true);
		
		ArrayList<String> itens = i.getStringArrayListExtra("Tweets");
		final StableArrayAdapter adapter = new StableArrayAdapter(this,android.R.layout.simple_list_item_1, itens);
		listView.setAdapter(adapter);

		}


	private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
	
    public boolean onCreateOptionsMenu(Menu menu) {
    	
    	Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
    	
    	if(Lingua==0){
    	getMenuInflater().inflate(R.menu.main, menu);
	    }else{
	    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
	    }
	    return true;
    }

	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click
					if(item.getItemId()==android.R.id.home){ 
						this.finish();
					}
	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    		return true;
	    	}
   

}
	


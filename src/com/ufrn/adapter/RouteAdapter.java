package com.ufrn.adapter;

import java.util.List;

import com.ufrn.domain.Route;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RouteAdapter extends BaseAdapter {

	private List<Route> routes;
	private Context ctx;
	
	public RouteAdapter(Context ctx, List<Route> routes) {
		this.routes = routes;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return routes.size();
	}

	@Override
	public Object getItem(int position) {
		return routes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		Route route = routes.get(position);
		TextView txtDate = new TextView(ctx);
		txtDate.setText(route.getDate());
		txtDate.setTextSize(22);
		
		return txtDate;
	}

}

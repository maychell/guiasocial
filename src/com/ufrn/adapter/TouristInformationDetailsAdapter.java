package com.ufrn.adapter;

import java.util.List;

import com.ufrn.domain.Subinformation_details;
import com.ufrn.domain.Touristinformation;
import com.ufrn.domain.TouristinformationDetails;
import com.ufrn.guiasocial.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TouristInformationDetailsAdapter extends BaseAdapter{
	private Context ctx;
	private List<TouristinformationDetails> TouristInformationsDetails;

	public TouristInformationDetailsAdapter(Context ctx, List<TouristinformationDetails> subtypeInformations) {
		this.TouristInformationsDetails = subtypeInformations;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return TouristInformationsDetails.size();
	}

	@Override
	public Object getItem(int position) {
		return TouristInformationsDetails.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.activity_subinformations_details, null);
		
		TouristinformationDetails localization = (TouristinformationDetails) getItem(position);
		TextView txt_subtype_information_name_value = (TextView) view.findViewById(R.id.txt_subtype_information_name_value);
		TextView txt_subtype_information_description_value = (TextView) view.findViewById(R.id.txt_subtype_information_description_value);
		TextView txt_subtype_information_endereco_value = (TextView) view.findViewById(R.id.txt_subtype_information_endereco_value);
		
		txt_subtype_information_name_value.setText(localization.getNome());
		txt_subtype_information_description_value.setText(localization.getDescricao());
		txt_subtype_information_endereco_value.setText(localization.getEndereco());
		
		return view;
	}

}

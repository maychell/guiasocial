package com.ufrn.adapter;

import java.util.List;

import com.ufrn.domain.Subinformation_details;
import com.ufrn.domain.Touristinformation;
import com.ufrn.guiasocial.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TouristInformationAdapter extends BaseAdapter{
	
	private Context ctx;
	private List<Touristinformation> TouristInformations;

	public TouristInformationAdapter(Context ctx, List<Touristinformation> subtypeInformations) {
		this.TouristInformations = subtypeInformations;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return TouristInformations.size();
	}

	@Override
	public Object getItem(int position) {
		return TouristInformations.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.activity_tourist_information_row, null);
		
		Touristinformation localization = TouristInformations.get(position);
		TextView txt_tourist_name_value = (TextView) view.findViewById(R.id.txt_tourist_information_name_value);
		TextView txt_tourist_description_value = (TextView) view.findViewById(R.id.txt_tourist_information_description_value);
		
		txt_tourist_description_value.setText(localization.getMini_descricao());
		txt_tourist_name_value.setText(localization.getNome_info());
		
		return view;
	}

}

package com.ufrn.adapter;

import java.util.List;
import com.ufrn.domain.Subinformation_details;
import com.ufrn.guiasocial.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Subinformation_detailsAdapter extends BaseAdapter {

	private Context ctx;
	private List<Subinformation_details> subtypeInformationsDetails;
	
	/**
	 * Constructor
	 */
	public Subinformation_detailsAdapter(Context ctx, List<Subinformation_details> subtypeInformations) {
		this.subtypeInformationsDetails = subtypeInformations;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return subtypeInformationsDetails.size();
	}

	@Override
	public Object getItem(int position) {
		return subtypeInformationsDetails.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.activity_subinformations_row, null);
		
		Subinformation_details localization = subtypeInformationsDetails.get(position);
		TextView txt_subtype_information_name_value = (TextView) view.findViewById(R.id.txt_subtype_information_name_value);
		TextView txt_subtype_information_description_value = (TextView) view.findViewById(R.id.txt_subtype_information_description_value);
		TextView txt_subtype_information_phone_value = (TextView) view.findViewById(R.id.txt_subtype_information_endereco_value);
		
		txt_subtype_information_name_value.setText(localization.getNome());
		txt_subtype_information_description_value.setText(localization.getNome());
		txt_subtype_information_phone_value.setText(localization.getNome());
		
		return view;
	}

}

package com.ufrn.adapter;

import java.util.List;
import com.ufrn.domain.Information;
import com.ufrn.guiasocial.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class InformationAdapter extends BaseAdapter {

	private Context ctx;
	private List<Information> loc;
	
	/**
	 * Constructor
	 */
	public InformationAdapter(Context ctx, List<Information> loc) {
		this.loc = loc;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return loc.size();
	}

	@Override
	public Object getItem(int position) {
		return loc.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.activity_informations, null);
		
		Information localization = loc.get(position);
		TextView txtOptions = (TextView) view.findViewById(R.id.tvInformationName);
		
		txtOptions.setText(localization.getNome());
		
		return view;
	}

}

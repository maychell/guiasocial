package com.ufrn.adapter;

import java.util.List;
import com.ufrn.domain.Subinformation;
import com.ufrn.guiasocial.R;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SubinformationAdapter extends BaseAdapter {

	private Context ctx;
	private List<Subinformation> subtypeInformations;
	
	/**
	 * Constructor
	 */
	public SubinformationAdapter(Context ctx, List<Subinformation> subtypeInformations) {
		this.subtypeInformations = subtypeInformations;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return subtypeInformations.size();
	}

	@Override
	public Object getItem(int position) {
		return subtypeInformations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(v == null){
			v = inflater.inflate(R.layout.activity_subinformations_row, null);
		}
		
		Subinformation localization = (Subinformation) getItem(position);
		
		Log.d ("VAZIOOOOOOOOOO", "Tamanho:" + subtypeInformations.size());
		TextView tvSubinformationName = (TextView) v.findViewById(R.id.tvSubinformationName);
		
		tvSubinformationName.setText(localization.getNome_subtipo());
		
		return v;
	}

}

package com.ufrn.domain;

public class RouteDetails extends Entity {

	//Facilitar a recuperacao de todas as colunas no banco.
	public static final String[] COLUMNS = new String[] {
		RouteDetails._ID, RouteDetails.LATITUDE, RouteDetails.LONGITUDE, RouteDetails.ID_ROUTE, RouteDetails.HORA
	};
	
	private double latitude;
	private double longitude;
	private long id_route;
	private String hora;
	
	/**
	 * Default construtor
	 */
	public RouteDetails() {}
	
	/**
	 * Construtor with primary key
	 */
	public RouteDetails(long id, double lat, double lng, long id_route, String hora){
		setId(id);
		this.latitude = lat;
		this.longitude = lng;
		this.id_route = id_route;
		this.hora = hora;
	}
	
	/**
	 * Construtor without primary key
	 */
	public RouteDetails(double lat, double lng, long id_route, String hora){
		this.latitude = lat;
		this.longitude = lng;
		this.id_route = id_route;
		this.hora = hora;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public long getId_route() {
		return id_route;
	}

	public void setId_route(long id_route) {
		this.id_route = id_route;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	//Nome da variavel no banco de dados
	public static final String _ID = "codigo_rota";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ID_ROUTE = "codigo_rota";
	public static final String HORA = "hora";
	
}

package com.ufrn.domain;

public class Subinformation_details extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
			public static final String[] COLUMNS = new String[] {
				Subinformation_details._ID, Subinformation_details._IDCODIGO_SUBTIPO, Subinformation_details._IDCODIGO_LINGUA, Subinformation_details.NOMEDETALHES
			};
		
			private String nome_detalhes;
			
			/**
			 * Default construtor
			 */
			public Subinformation_details() { }
			
			/**
 			 * Construtor with primary key
			 */
			public Subinformation_details(long id, long id2, long id3, String nome_detalhes){
				setId(id);
				setId(id2);
				setId(id3);
				this.nome_detalhes = nome_detalhes;
			}
			
			/**
			 * Construtor without primary key
			 */
			public Subinformation_details(String nome_detalhes){
				this.nome_detalhes = nome_detalhes;
			}
		
			public String getNome() {
				return nome_detalhes;
			}

			public void setNome(String nome_detalhes) {
				this.nome_detalhes = nome_detalhes;
			}

			//Nome da variável no banco de dados
			public static final String _ID = "codigo_tipo";
			public static final String _IDCODIGO_SUBTIPO = "codigo_subtipo";
			public static final String _IDCODIGO_LINGUA = "codigo_lingua";
			public static final String NOMEDETALHES = "nome_detalhes";
			
	}



package com.ufrn.domain;

public class Touristinformation extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
		public static final String[] COLUMNS = new String[] {
			Touristinformation.CODIGO_INFORMACAO, Touristinformation.CODIGO_TIPO, Touristinformation.CODIGO_SUBTIPO, Touristinformation.NOME_INFO, 
			Touristinformation.MINI_DESCRICAO
		};
	
		private String nome_info;
		private String mini_descricao;
		
		/**
		 * Default construtor
		 */
		public Touristinformation() { }
		
		/**
			 * Construtor with primary key
		 */
		public Touristinformation(long id, String nome_info, String descricao){
			setId(id);
			this.nome_info = nome_info;
			this.mini_descricao = descricao;
		}
		
		/**
		 * Construtor without primary key
		 */
		public Touristinformation(String nome_info){
			this.nome_info = nome_info;
		}
	
		public String getNome_info() {
			return nome_info;
		}
	
		public void setNome_info(String nome_info) {
			this.nome_info = nome_info;
		}
		
		public String getMini_descricao() {
			return mini_descricao;
		}

		public void setMini_descricao(String mini_descricao) {
			this.mini_descricao = mini_descricao;
		}
	
		//Nome da variável no banco de dados
		public static final String CODIGO_INFORMACAO = "codigo_informacao";
		public static final String CODIGO_TIPO = "codigo_tipo";
		public static final String CODIGO_SUBTIPO = "codigo_subtipo";
		public static final String NOME_INFO = "nome_info";
		public static final String MINI_DESCRICAO = "mini_descricao";
			
	}



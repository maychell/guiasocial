package com.ufrn.domain;

public class ProfileDetails extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
			public static final String[] COLUMNS = new String[] {
				ProfileDetails._IDCODIGO_PERFIL, ProfileDetails._IDCODIGO_LINGUA, ProfileDetails._NOME};
			
			private String nome;
			
			/**
			 * Default construtor
			 */
			public ProfileDetails() { }
			
			/**
 			 * Construtor with primary key
			 */
			public ProfileDetails(long id, long id2, String nome){
				setId(id);
				setId(id2);
				this.nome = nome;
			}
			
			/**
			 * Construtor without primary key
			 */
			public ProfileDetails(String nome){
				this.nome = nome;
			}
		
			public String getNome() {
				return nome;
			}

			public void setNome(String nome) {
				this.nome = nome;
			}

			//Nome da variável no banco de dados
			public static final String _IDCODIGO_PERFIL = "codigo_perfil";
			public static final String _IDCODIGO_LINGUA = "codigo_lingua";
			public static final String _NOME = "nome";
			
	}


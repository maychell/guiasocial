package com.ufrn.domain;

public class Route extends Entity {

	//Facilitar a recuperacao de todas as colunas no banco.
	public static final String[] COLUMNS = new String[] {
		Route._ID, Route.DATE
	};
	
	private String date;
	
	public Route() {}
	
	/**
	 * Constructor with primary key
	 * @param date
	 */
	public Route(long id, String date) {
		setId(id);
		this.date = date;
	}
	
	/**
	 * Constructor without primary key
	 * @param date
	 */
	public Route(String date) {
		this.date = date;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public static final String _ID = "codigo_rota";
	public static final String DATE = "data";
}

package com.ufrn.domain;

/**
 * @author maychell
 * created at 11.04.2014
 */
public class Language extends Entity {

	//Facilitar a recuperação de todas as colunas no banco.
	public static final String[] COLUMNS = {
		Language._ID, Language.NAME
	};
	
	private String name;

	/**
	 * Default constructor
	 */
	public Language() { }
	
	/**
	 * Constructor with primary key
	 * @param id
	 * @param name
	 */
	public Language(long id, String name){
		setId(id);
		this.name = name;
	}
	
	/**
	 * Constructor without primary key
	 * @param name
	 */
	public Language(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	//Nome da variável no banco de dados
	public static final String NAME = "nome";
	public static final String _ID = "codigo_lingua";
	
}

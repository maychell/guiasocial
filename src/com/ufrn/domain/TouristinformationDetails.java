package com.ufrn.domain;

public class TouristinformationDetails extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
		public static final String[] COLUMNS = new String[] {
			TouristinformationDetails.CODIGO_DETALHES, TouristinformationDetails.CODIGO_INFORMACAO, TouristinformationDetails.CODIGO_LINGUA, TouristinformationDetails.NOME, TouristinformationDetails.DESCRICAO, TouristinformationDetails.ENDERECO
		};
	
		private String nome;
		private String descricao;
		private String endereco;
		
		/**
		 * Default construtor
		 */
		public TouristinformationDetails() { }
		
		/**
			 * Construtor with primary key
		 */
		public TouristinformationDetails(long id, String nome, String descricao, String endereco){
			setId(id);
			this.nome = nome;
			this.descricao = descricao;
			this.endereco = endereco;
		}
		
		/**
		 * Construtor without primary key
		 */
		public TouristinformationDetails(String nome){
			this.nome = nome;
		}
	
		public String getNome() {
			return nome;
		}
	
		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public String getDescricao() {
			return descricao;
		}
	
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public String getEndereco() {
			return endereco;
		}
	
		public void setEndereco(String endereco) {
			this.endereco = endereco;
		}
		
		//Nome da variável no banco de dados
		public static final String CODIGO_DETALHES = "codigo_detalhes";
		public static final String CODIGO_INFORMACAO = "codigo_informacao";
		public static final String CODIGO_LINGUA = "codigo_lingua";
		public static final String NOME = "nome";
		public static final String DESCRICAO = "descricao";
		public static final String ENDERECO = "endereco";
	}


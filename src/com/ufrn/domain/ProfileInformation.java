package com.ufrn.domain;

public class ProfileInformation extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
			public static final String[] COLUMNS = new String[] {
				ProfileInformation._IDCODIGO_PERFIL_INFORMACAO, ProfileInformation._IDCODIGO_PERFIL, ProfileInformation._IDCODIGO_LINGUA};
			
			/**
			 * Default construtor
			 */
			public ProfileInformation() { }
			
			/**
 			 * Construtor with primary key
			 */
			public ProfileInformation(long id, long id2, long id3){
				setId(id);
				setId(id2);
				setId(id3);
			}
			
			

			//Nome da variável no banco de dados
			public static final String _IDCODIGO_PERFIL_INFORMACAO = "codigo_perfil_informacao";
			public static final String _IDCODIGO_PERFIL = "codigo_perfil";
			public static final String _IDCODIGO_LINGUA = "codigo_lingua";
			
	}


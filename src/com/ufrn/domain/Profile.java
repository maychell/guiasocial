package com.ufrn.domain;

public class Profile extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
			public static final String[] COLUMNS = new String[] {
				Profile._IDCODIGO_PERFIL};
			
			/**
			 * Default construtor
			 */
			public Profile() { }
			
			/**
 			 * Construtor with primary key
			 */
			public Profile(long id){
				setId(id);
			}

			//Nome da variável no banco de dados
			public static final String _IDCODIGO_PERFIL = "codigo_perfil";
			
	}


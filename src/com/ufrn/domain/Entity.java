package com.ufrn.domain;

/**
 * @author maychell
 * created at 11.04.2014
 */
public class Entity {

	private long _id;

	public long getId() {
		return _id;
	}

	public void setId(long _id) {
		this._id = _id;
	}
	
}

package com.ufrn.domain;

public class Subinformation extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
		public static final String[] COLUMNS = new String[] {
			Subinformation._ID, Subinformation.CODIGO_TIPO, Subinformation.NOME
		};
	
		private String nome_subtipo;
		
		/**
		 * Default construtor
		 */
		public Subinformation() { }
		
		/**
			 * Construtor with primary key
		 */
		public Subinformation(long id, String nome){
			setId(id);
			setNome_subtipo(nome);
		}
		
		/**
		 * Construtor without primary key
		 */
		public Subinformation(String nome){
			this.nome_subtipo = nome;
		}
		
		public String getNome_subtipo() {
			return nome_subtipo;
		}
	
		public void setNome_subtipo(String nome_subtipo) {
			this.nome_subtipo = nome_subtipo;
		}
	
		//Nome da variável no banco de dados
		public static final String _ID = "codigo_subtipo";
		public static final String CODIGO_TIPO = "codigo_tipo";
		public static final String NOME = "nome_subtipo";
			
	}


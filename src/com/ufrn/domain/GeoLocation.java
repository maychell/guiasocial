package com.ufrn.domain;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * @author maychell
 * created at 11.04.2014
 */
public class GeoLocation extends Entity {

	//Facilitar a recuperação de todas as colunas no banco.
	public static final String[] COLUMNS = new String[] {
		GeoLocation._ID, GeoLocation.PLACE_NAME, GeoLocation.LATITUDE, GeoLocation.LONGITUDE,
		GeoLocation.ADDRESS, GeoLocation.ID_GOOGLE_LOCATION, GeoLocation.ID_SUBTYPE_INFORMATION,
		GeoLocation.PHONE_NUMBER
	};
	
	private String place_name;
	private double latitude;
	private double longitude;
	private String address;
	private String id_google_location;
	private long id_subtype_information;
	private String phone_number;
	
	/**
	 * Default construtor
	 */
	public GeoLocation() { }
	
	/**
	 * Construtor with primary key
	 */
	public GeoLocation(long id, String place_name, double latitude, double longitude,
			String address, String id_google_location, long id_subtype_information, String phone_number){
		setId(id);
		this.place_name = place_name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.id_google_location = id_google_location;
		this.id_subtype_information = id_subtype_information;
		this.phone_number = phone_number; 
	}
	
	/**
	 * Construtor without primary key
	 */
	public GeoLocation(String place_name, double latitude, double longitude,
			String address, String id_google_location, long id_subtype_information, String phone_number){
		this.place_name = place_name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.id_google_location = id_google_location;
		this.id_subtype_information = id_subtype_information;
		this.phone_number = phone_number;
	}
	
	public String getPlaceName() {
		return place_name;
	}
	public void setPlaceName(String place_name) {
		this.place_name = place_name;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIdGoogleLocation() {
		return id_google_location;
	}
	public void setIdGoogleLocation(String id_google_location) {
		this.id_google_location = id_google_location;
	}
	public long getIdSubtypeInformation() {
		return id_subtype_information;
	}
	public void setIdSubtypeInformation(long id_subtypeInformation) {
		this.id_subtype_information = id_subtypeInformation;
	}
	public String getPhoneNumber() {
		return phone_number;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phone_number = phoneNumber;
	}

	//Nome da variável no banco de dados
	public static final String _ID = "codigo_informacao";
	public static final String PLACE_NAME = "place_name";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String ADDRESS = "address";
	public static final String ID_GOOGLE_LOCATION = "id_google_location";
	public static final String ID_SUBTYPE_INFORMATION = "id_subtipo_informacao";
	public static final String PHONE_NUMBER = "telefone";
	
	public void deserialize(DataInputStream in) throws IOException {
		place_name = in.readUTF();
		latitude = in.readFloat();
		longitude = in.readFloat();
		address = in.readUTF();
		id_google_location = in.readUTF();
		id_subtype_information = in.readLong();
		phone_number = in.readUTF();
	}
	
}

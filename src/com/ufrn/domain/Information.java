package com.ufrn.domain;

public class Information extends Entity{
	
	//Facilitar a recuperação de todas as colunas no banco.
			public static final String[] COLUMNS = new String[] {
				Information._ID, Information.NOME
			};
		
			private String nome;
			
			/**
			 * Default construtor
			 */
			public Information() { }
			
			/**
 			 * Construtor with primary key
			 */
			public Information(long id, String nome){
				setId(id);
				this.nome = nome;
			}
			
			/**
			 * Construtor without primary key
			 */
			public Information(String nome){
				this.nome = nome;
			}
		
			public String getNome() {
				return nome;
			}

			public void setNome(String nome) {
				this.nome = nome;
			}

			//Nome da variável no banco de dados
			public static final String _ID = "codigo_tipo";
			public static final String NOME = "nome";
			
	}


package com.ufrn.layoutstab;


import java.util.ArrayList;
import java.util.List;

import com.ufrn.adapter.SubinformationAdapter;
import com.ufrn.dao.DaoSubinformation;
import com.ufrn.domain.Subinformation;
import com.ufrn.guiasocial.MapsActivity;
import com.ufrn.guiasocial.RoutesActivity;
import com.ufrn.guiasocial.ServicesActivity;
import com.ufrn.guiasocial.TwitterActivity;
import com.ufrn.guiasocial.R;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

@SuppressLint("NewApi")
public class ActivityTabs extends FragmentActivity implements OnTabChangeListener, OnPageChangeListener{
	int Lingua,tab;
	private TabsPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TabHost mTabHost;
    
    private TextView txtSubinformationName;
	private DaoSubinformation dao;
	private ArrayList<Subinformation> subinformation;
	private long idSubtypeInformation;
	
    @SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.activity_tabs);
		
        ActionBar bar = getActionBar();
		 bar.setDisplayUseLogoEnabled(false);
		 bar.setDisplayShowTitleEnabled(false);
		 bar.setDisplayHomeAsUpEnabled(true);
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#edf0f0"));
		bar.setBackgroundDrawable(colorDrawable);
		
		 Intent it = getIntent();
		 tab =it.getIntExtra("tab", tab);
		
		mViewPager = (ViewPager) findViewById(R.id.viewpager);

		
        // Tab Initialization
        initialiseTabHost();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        // Fragments and ViewPager Initialization

        mViewPager.setAdapter(mAdapter);
        mViewPager.setOnPageChangeListener(ActivityTabs.this);
        
        mTabHost.setCurrentTab(tab);
        
        

	}
    
	
	// Method to add a TabHost
    private static void AddTab(ActivityTabs activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
        if(tag.equals("aaa")){        		
    		mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.selecionado);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.nselecionado);
            mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.nselecionado);                   
    	}else if(tag.equals("bbb")){
    		//se a segunda aba for selecionada
    		mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.nselecionado);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.selecionado);
            mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.nselecionado); 
    	}else if(tag.equals("ccc")){
    		//se a terceira aba for selecionada            		
    		mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.nselecionado);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.nselecionado);
            mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.selecionado); 
    	}
        
        
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
    }

    @Override
        public void onPageSelected(int arg0) {
    }


  
    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        
        Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
        tab =it.getIntExtra("tab", tab);
        mTabHost.setup();
        if(Lingua==1){
        // TODO Put here your Tabs
	        ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("aaa").setIndicator("Conhe�a Natal"));
	        ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("bbb").setIndicator("Restaurantes e Bares"));
	        ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("ccc").setIndicator("Hot�is e Pousadas"));
	        
	        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.selecionado);
	        mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.nselecionado);
	        mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.nselecionado); 
	
	        mTabHost.setOnTabChangedListener(this);
        }else{
            ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("aaa").setIndicator("Meet Natal"));
            ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("bbb").setIndicator("Snack Bar and Restaurants"));
            ActivityTabs.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("ccc").setIndicator("Hotels"));
            
            mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.selecionado);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.nselecionado);
            mTabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.nselecionado); 

            mTabHost.setOnTabChangedListener(this);	
        }
        
    }
	
    
    public boolean onCreateOptionsMenu(Menu menu) {
    	
    	Intent it = getIntent();
        Lingua =it.getIntExtra("Lingua", Lingua);
    	
    	if(Lingua==0){
    	getMenuInflater().inflate(R.menu.main, menu);
	    }else{
	    getMenuInflater().inflate(R.menu.activity_main_actions, menu);	
	    }
	    return true;
    }
    
	   public boolean onOptionsItemSelected(MenuItem item) {
	        // Take appropriate action for each action item click
					if(item.getItemId()==android.R.id.home){ 
						this.finish();
					}
	    			if(item.getItemId()==R.id.twitter_eng){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services_eng){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.meet_natal_eng){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps_eng){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.twitter){
	    				Intent it = new Intent(this, TwitterActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it); 
	    			}
	    			if(item.getItemId()==R.id.services){
	    				Intent it = new Intent(this, ServicesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    				
	    			}
	    			if(item.getItemId()==R.id.meet_natal){
	    				Intent it = new Intent(this, ActivityTabs.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.maps){
	    				Intent it = new Intent(this, MapsActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    			if(item.getItemId()==R.id.routes_eng){
	    				Intent it = new Intent(this, RoutesActivity.class);  
	    				it.putExtra("Lingua", Lingua);
	    				startActivity(it);
	    			}
	    		return true;
	    	}
    

}

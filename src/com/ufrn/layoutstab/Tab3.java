package com.ufrn.layoutstab;


import java.util.ArrayList;
import java.util.List;

import com.ufrn.adapter.SubinformationAdapter;
import com.ufrn.dao.DaoSubinformation;
import com.ufrn.domain.Subinformation;
import com.ufrn.guiasocial.R;
import com.ufrn.guiasocial.TouristInformationActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class Tab3 extends ListFragment {

	private TextView txtSubinformationName;
	private DaoSubinformation dao;
	private List<Subinformation> subinformation;
	private long idSubtypeInformation;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		dao = new DaoSubinformation(this.getActivity());
		View rootView = inflater.inflate(R.layout.tab3_view, container, false);
		list();
		return rootView;
		
	}
	
	private void list() {
		subinformation = dao.getAllSubtipoInformacao(3);
		if(subinformation != null && subinformation.size() > 0)
			this.setListAdapter(new SubinformationAdapter(this.getActivity(), subinformation));
		else
			txtSubinformationName.setText("Nenhum registro encontrado");
	}
	
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Subinformation t = subinformation.get(position);
		Intent i = new Intent(this.getActivity(), TouristInformationActivity.class);
		i.putExtra("id_tourist_information", t.getId());
		startActivity(i);
		
	}
	
}
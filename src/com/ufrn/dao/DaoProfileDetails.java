package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.ProfileDetails;


	public class DaoProfileDetails extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Perfil_Detalhes";
		private Context ctx;
		
		public DaoProfileDetails(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<ProfileDetails> getAllTipoInformacao(){
			try {
				Cursor c = getAllCursors();
				List<ProfileDetails> profileDetails = new ArrayList<ProfileDetails>();
				if(c.moveToFirst()){
					
					int idxcodigoPerfil = c.getColumnIndex(ProfileDetails._IDCODIGO_PERFIL);
					int idxcodigoLingua = c.getColumnIndex(ProfileDetails._IDCODIGO_LINGUA);
					int idxname = c.getColumnIndex(ProfileDetails._NOME);
					
					do{
						ProfileDetails pro = new ProfileDetails(c.getLong(idxcodigoPerfil), c.getLong(idxcodigoLingua), c.getString(idxname));
						profileDetails.add(pro);
					} while(c.moveToNext());
				}
				return profileDetails;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors() {
			try{
				return db.query(TABLE_NAME, ProfileDetails.COLUMNS, null, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
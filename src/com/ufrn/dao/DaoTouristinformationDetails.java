package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Touristinformation;
import com.ufrn.domain.TouristinformationDetails;


	public class DaoTouristinformationDetails extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Informacao_Turistica_Detalhes";
		private Context ctx;
		
		public DaoTouristinformationDetails(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<TouristinformationDetails> getAllTipoInformacao(long id){
			try {
				Cursor c = getAllCursors(id);
				List<TouristinformationDetails> touristinfomacoesdetails = new ArrayList<TouristinformationDetails>();
				/*if(c.moveToFirst()){
					int idxcodigoDetalhes = c.getColumnIndex(TouristinformationDetails.CODIGO_DETALHES);
					String idxname = TouristinformationDetails.NOME;
					String idxdescricao = TouristinformationDetails.DESCRICAO;
					String idxdendereco = TouristinformationDetails.ENDERECO;
					
					do{
						TouristinformationDetails touristdetails = new TouristinformationDetails(c.getLong(idxcodigoDetalhes), idxname, idxdescricao, idxdendereco);
						touristinfomacoesdetails.add(touristdetails);
					} while(c.moveToNext());
				}*/
				
				c.moveToFirst();
			    while (!c.isAfterLast()) {
			    	int idxcodigoDetalhes = c.getColumnIndex(TouristinformationDetails.CODIGO_DETALHES);
			    	int idxn = c.getColumnIndex(TouristinformationDetails.NOME);
			    	int idxd = c.getColumnIndex(TouristinformationDetails.DESCRICAO);
			    	int idxe = c.getColumnIndex(TouristinformationDetails.ENDERECO);
			    	
					String idxname = c.getString(idxn);
					String idxdescricao = c.getString(idxd);
					String idxdendereco = c.getString(idxe);
					TouristinformationDetails touristdetails = new TouristinformationDetails(c.getLong(idxcodigoDetalhes), idxname, idxdescricao, idxdendereco);
					touristinfomacoesdetails.add(touristdetails);
			      c.moveToNext();
			    }
				
				return touristinfomacoesdetails;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors(long id) {
			try{
				return db.query(TABLE_NAME, TouristinformationDetails.COLUMNS, TouristinformationDetails.CODIGO_INFORMACAO + "=" + id, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
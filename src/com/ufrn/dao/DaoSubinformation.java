package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.widget.Toast;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Subinformation;


	public class DaoSubinformation extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Subtipo_Informacao";
		private Context ctx;
		
		public DaoSubinformation(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			return 0;
		}

		@Override
		public long insert(Object obj) {
			return 0;
		}

		@Override
		public int update(Object obj) {
			return 0;
		}
		
		public List<Subinformation> getAllSubtipoInformacao(long idTipo){
			try {
				Cursor c = getAllCursors(idTipo);
				List<Subinformation> tipoInformacoes = new ArrayList<Subinformation>();
				/*if(c.moveToFirst()){

					int idxcodigoTipo = c.getColumnIndex(Subinformation._ID);
					int idxcolunanome = c.getColumnIndex(Subinformation.NOME);
					String idxname = c.getString(idxcolunanome);
					
					do{
						Subinformation subinformation = new Subinformation(c.getLong(idxcodigoTipo), idxname);
						tipoInformacoes.add(subinformation);
					} while(c.moveToNext());
				}*/
				c.moveToFirst();
			    while (!c.isAfterLast()) {
			    	int idxcodigoTipo = c.getColumnIndex(Subinformation._ID);
					int idxcolunanome = c.getColumnIndex(Subinformation.NOME);
					String idxname = c.getString(idxcolunanome);
			    	Subinformation subinformation = new Subinformation(c.getLong(idxcodigoTipo), idxname);
					tipoInformacoes.add(subinformation);
			      c.moveToNext();
			    }
				
				
				return tipoInformacoes;
			} catch(Exception e){
				Log.e("SubInfomation", "Erro DaoSubInformation" + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors(long idTipo) {
			try{
				return db.query(TABLE_NAME, Subinformation.COLUMNS, Subinformation.CODIGO_TIPO + "=" + idTipo, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("SubTipoInforma��o", "Error DaoSubinformation" + e.toString());
				return null;
			}
		}
}
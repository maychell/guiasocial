package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.ProfileInformation;;


	public class DaoProfileInformation extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Perfil_Informacao";
		private Context ctx;
		
		public DaoProfileInformation(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<ProfileInformation> getAllTipoInformacao(){
			try {
				Cursor c = getAllCursors();
				List<ProfileInformation> profilmeInformation = new ArrayList<ProfileInformation>();
				if(c.moveToFirst()){
					
					int idxcodigoPerfilInformacao = c.getColumnIndex(ProfileInformation._IDCODIGO_PERFIL_INFORMACAO);
					int idxcodgoPerfil= c.getColumnIndex(ProfileInformation._IDCODIGO_PERFIL);
					int idxcodgoLingua = c.getColumnIndex(ProfileInformation._IDCODIGO_LINGUA);
					
					do{
						ProfileInformation pro = new ProfileInformation(c.getLong(idxcodigoPerfilInformacao), c.getLong(idxcodgoPerfil), c.getLong(idxcodgoLingua));
						profilmeInformation.add(pro);
					} while(c.moveToNext());
				}
				return profilmeInformation;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors() {
			try{
				return db.query(TABLE_NAME, ProfileInformation.COLUMNS, null, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
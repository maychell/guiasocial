package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.RouteDetails;

public class DaoRouteDetails extends AbstractRepository {
	
	private static final String TAG = "DaoRouteDetails";
	protected static final String TABLE_NAME = "Rotas_Detalhes";
	private Context ctx;
	
	public DaoRouteDetails(Context ctx){
		this.ctx = ctx;
		db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
	}
	
	public DaoRouteDetails(){}
	
	@Override
	public long save(Object obj){
		RouteDetails routeDetails = (RouteDetails) obj;
		long id = routeDetails.getId();
		if(id == 0)
			id = insert(routeDetails);
		else
			update(routeDetails);
		return id;
	}

	/**
	 * To insert an RouteDetails on local database
	 */
	@Override
	public long insert(Object obj) {
		RouteDetails routeDetails = (RouteDetails) obj;
		ContentValues values = new ContentValues();
		values.put(RouteDetails.LATITUDE, routeDetails.getLatitude());
		values.put(RouteDetails.LONGITUDE, routeDetails.getLongitude());
		values.put(RouteDetails.ID_ROUTE, routeDetails.getId_route());
		values.put(RouteDetails.HORA, routeDetails.getHora());
		return insert(ctx, TABLE_NAME, values);
	}

	/**
	 * To update an Route on local database
	 */
	@Override
	public int update(Object obj) {
		RouteDetails routeDetails = (RouteDetails) obj;
		ContentValues values = new ContentValues();
		values.put(RouteDetails.LATITUDE, routeDetails.getLatitude());
		values.put(RouteDetails.LONGITUDE, routeDetails.getLongitude());
		values.put(RouteDetails.ID_ROUTE, routeDetails.getId_route());
		values.put(RouteDetails.HORA, routeDetails.getHora());
		
		String _id = String.valueOf(routeDetails.getId());
		String where = RouteDetails._ID + "=?";
		String[] whereArgs = new String[] { _id };
		return update(ctx, TABLE_NAME, values, where, whereArgs);
	}
	
	/**
	 * To get all RouteDetails from local database
	 * @return
	 */
	public List<RouteDetails> getAllRoutes(){
		try {
			Cursor c = getAllCursors();
			List<RouteDetails> routesDetails = new ArrayList<RouteDetails>();
			if(c.moveToFirst()){
				int idxid = c.getColumnIndex(RouteDetails._ID);
				int idxlat = c.getColumnIndex(RouteDetails.LATITUDE);
				int idxlng = c.getColumnIndex(RouteDetails.LONGITUDE);
				int ididRoute = c.getColumnIndex(RouteDetails.ID_ROUTE);
				int idxhora = c.getColumnIndex(RouteDetails.HORA);
				do{
					RouteDetails routeDetails = new RouteDetails(c.getLong(idxid), c.getDouble(idxlat), c.getDouble(idxlng), c.getLong(ididRoute), c.getString(idxhora));
					routesDetails.add(routeDetails);
				} while(c.moveToNext());
			}
			return routesDetails;
		} catch(Exception e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS DETALHES: " + e.toString());
			return null;
		}
	}
	
	/** 
	 * To get an cursor with all the RouteDetails
	 * @return Cursor */
	private Cursor getAllCursors(){
		try{
			return db.query(TABLE_NAME, RouteDetails.COLUMNS, null, null, null, null, null);
		}
		catch (SQLException e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS DETALHES: " + e.toString());
			return null;
		}
	}
	
	public List<RouteDetails> getRouteDetailsByRoute(long id_route){
		try {
			Cursor c = db.query(TABLE_NAME, RouteDetails.COLUMNS, RouteDetails.ID_ROUTE + "=" + id_route, null, null, null, null);
			List<RouteDetails> routesDetails = new ArrayList<RouteDetails>();
			if(c.moveToFirst()){
				int idxid = c.getColumnIndex(RouteDetails._ID);
				int idxlat = c.getColumnIndex(RouteDetails.LATITUDE);
				int idxlng = c.getColumnIndex(RouteDetails.LONGITUDE);
				int ididRoute = c.getColumnIndex(RouteDetails.ID_ROUTE);
				int idxhour = c.getColumnIndex(RouteDetails.HORA);
				do{
					RouteDetails routeDetails = new RouteDetails(c.getLong(idxid), c.getDouble(idxlat), c.getDouble(idxlng), c.getLong(ididRoute), c.getString(idxhour));
					routesDetails.add(routeDetails);
				} while(c.moveToNext());
			}
			return routesDetails;
		} catch(Exception e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS DETALHES: " + e.toString());
			return null;
		}
	}
	
	public void dump() {
		String query = "SELECT * FROM " + TABLE_NAME;
		Cursor cursor = db.rawQuery(query, null);
		
		Log.d(TAG, "Dumping...");
		
		while (cursor.moveToNext()) {
			Log.d(TAG, "[" + cursor.getDouble(1) + ", " + cursor.getDouble(2) + "]");
		}				
	}	
	
	public boolean latlngExist(double d, double e){
		boolean existe = false;
		String query = "SELECT latitude, longitude FROM " + TABLE_NAME;
		Cursor cursor = db.rawQuery(query, null);
		
		Log.d(TAG, "Searching...");
		
		while (cursor.moveToNext()) {
			if(d == cursor.getDouble(0) && e == cursor.getDouble(1) ){
				existe = true;
			}
		}		
		return existe;
	}
}
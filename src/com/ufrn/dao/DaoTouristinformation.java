package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Subinformation;
import com.ufrn.domain.Touristinformation;


	public class DaoTouristinformation extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Informacao_Turistica";
		private Context ctx;
		
		public DaoTouristinformation(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<Touristinformation> getAllTipoInformacao(long idTipo){
			try {
				Cursor c = getAllCursors(idTipo);
				List<Touristinformation> Touristinformacoes = new ArrayList<Touristinformation>();
				/*if(c.moveToFirst()){
					int idxcodigoInformacao = c.getColumnIndex(Touristinformation.CODIGO_INFORMACAO);
					int idxnome = c.getColumnIndex(Touristinformation.NOME_INFO);
					int idxdesc = c.getColumnIndex(Touristinformation.MINI_DESCRICAO);
					String idxcodigoNomeinfo = c.getString(idxnome);
					String idxminidescricao = c.getString(idxdesc) ;
					
					do{
						Touristinformation tourist = new Touristinformation(c.getLong(idxcodigoInformacao), idxcodigoNomeinfo, idxminidescricao);
						Touristinformacoes.add(tourist);
					} while(c.moveToNext());
				}*/
				c.moveToFirst();
			    while (!c.isAfterLast()) {
			    	int idxcodigoInformacao = c.getColumnIndex(Touristinformation.CODIGO_INFORMACAO);
					int idxnome = c.getColumnIndex(Touristinformation.NOME_INFO);
					int idxdesc = c.getColumnIndex(Touristinformation.MINI_DESCRICAO);
					String idxcodigoNomeinfo = c.getString(idxnome);
					String idxminidescricao = c.getString(idxdesc) ;
					Touristinformation tourist = new Touristinformation(c.getLong(idxcodigoInformacao), idxcodigoNomeinfo, idxminidescricao);
					Touristinformacoes.add(tourist);
			      c.moveToNext();
			    }
				return Touristinformacoes;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors(long idTipo) {
			try{
				return db.query(TABLE_NAME, Touristinformation.COLUMNS, Touristinformation.CODIGO_SUBTIPO + "=" + idTipo, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
package com.ufrn.dao;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Subinformation_details;


	public class DaoSubinformation_details extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Subtipo_Informacao_Detalhes";
		private Context ctx;
		
		public DaoSubinformation_details(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<Subinformation_details> getAllTipoInformacao(){
			try {
				Cursor c = getAllCursors();
				List<Subinformation_details> subTipoInformacaoDetalhesS = new ArrayList<Subinformation_details>();
				if(c.moveToFirst()){
					int idxcodigoTipo = c.getColumnIndex(Subinformation_details._ID);
					int idxcodigoSubTipo = c.getColumnIndex(Subinformation_details._IDCODIGO_SUBTIPO);
					int idxcodigoLingua = c.getColumnIndex(Subinformation_details._IDCODIGO_LINGUA);
					int idxnameDetalhes = c.getColumnIndex(Subinformation_details.NOMEDETALHES);
					
					do{
						Subinformation_details subinformation_details = new Subinformation_details(c.getLong(idxcodigoTipo), c.getLong(idxcodigoSubTipo), c.getLong(idxcodigoLingua), c.getString(idxnameDetalhes));
						subTipoInformacaoDetalhesS.add(subinformation_details);
					} while(c.moveToNext());
				}
				return subTipoInformacaoDetalhesS;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors() {
			try{
				return db.query(TABLE_NAME, Subinformation_details.COLUMNS, null, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("SubTipoInformacaoDetalhes", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
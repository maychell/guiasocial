package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Profile;


	public class DaoProfile extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Perfil";
		private Context ctx;
		
		public DaoProfile(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<Profile> getAllTipoInformacao(){
			try {
				Cursor c = getAllCursors();
				List<Profile> profile = new ArrayList<Profile>();
				if(c.moveToFirst()){
					
					int idxcodigoPerfil = c.getColumnIndex(Profile._IDCODIGO_PERFIL);
					
					do{
						Profile pro = new Profile(c.getLong(idxcodigoPerfil));
						profile.add(pro);
					} while(c.moveToNext());
				}
				return profile;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors() {
			try{
				return db.query(TABLE_NAME, Profile.COLUMNS, null, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
package com.ufrn.dao;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Language;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * @author maychell
 * created at 11.04.2014
 */
public class DaoLanguage extends AbstractRepository {
	
	protected static final String TABLE_NAME = "Lingua";
	private Context ctx;
	
	public DaoLanguage(Context ctx){
		this.ctx = ctx;
		db = ctx.openOrCreateDatabase(TABLE_NAME, Context.MODE_PRIVATE, null);
	}
	
	/**
	 * To save a language on local database
	 */
	@Override
	public long save(Object obj){
		Language lang = (Language) obj;
		long id = lang.getId();
		if(id == 0)
			id = insert(lang);
		else
			update(lang);
		return id;
	}
	
	/**
	 * To insert a language on local database
	 */
	@Override
	public long insert(Object obj){
		Language lang = (Language) obj;
		ContentValues values = new ContentValues();
		values.put(Language.NAME, lang.getName());
		
		return insert(ctx, TABLE_NAME, values);
	}
	
	/**
	 * To update a language on local database
	 */
	@Override
	public int update(Object obj){
		Language lang = (Language) obj;
		ContentValues values = new ContentValues();
		values.put(Language.NAME, lang.getName());
		
		String _id = String.valueOf(lang.getId());
		String where = Language._ID + "=?";
		String[] whereArgs = new String[] { _id };
		return update(ctx, TABLE_NAME, values, where, whereArgs);
	}
	
	/** 
	 * To get a language by id from database
	 * @param id
	 * @return */
	public Language findByPrimaryKey(long id){
		Cursor c = db.query(true, TABLE_NAME, Language.COLUMNS, Language._ID + "=" + id, null, null, null, null, null);
		if(c.getCount()>0){
			int idxid = c.getColumnIndex(Language._ID);
			int idxname = c.getColumnIndex(Language.NAME);
			c.moveToFirst();
			Language lang = new Language(c.getLong(idxid), c.getString(idxname));
			return lang;
		}
		return null;
	}
	
}
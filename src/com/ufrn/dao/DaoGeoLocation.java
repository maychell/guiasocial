package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.GeoLocation;

/**
 * @author maychell
 * created at 11.04.2014
 */
public class DaoGeoLocation extends AbstractRepository {

	protected static final String TABLE_NAME = "Informacoes_Posicao_Geograficas";
	private Context ctx;
	
	/**
	 * Constructor of DaoGeoLocation
	 * @param ctx
	 */
	public DaoGeoLocation(Context ctx) {
		this.ctx = ctx;
		db = ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
	}
	
	/**
	 * To save an GeoLocation on local database
	 */
	@Override
	public long save(Object obj) {
		GeoLocation geo = (GeoLocation) obj;
		long id = geo.getId();
		if(id == 0)
			id = insert(geo);
		else
			update(geo);
		return id;
	}

	/**
	 * To insert an GeoLocation on local database
	 */
	@Override
	public long insert(Object obj) {
		GeoLocation geo = (GeoLocation) obj;
		ContentValues values = new ContentValues();
		values.put(GeoLocation.PLACE_NAME, geo.getPlaceName());
		values.put(GeoLocation.LATITUDE, geo.getLatitude());
		values.put(GeoLocation.LONGITUDE, geo.getLongitude());
		values.put(GeoLocation.ADDRESS, geo.getAddress());
		values.put(GeoLocation.ID_GOOGLE_LOCATION, geo.getIdGoogleLocation());
		values.put(GeoLocation.ID_SUBTYPE_INFORMATION, geo.getIdSubtypeInformation());
		values.put(GeoLocation.PHONE_NUMBER, geo.getPhoneNumber());
		return insert(ctx, TABLE_NAME, values);
	}

	/**
	 * To update an GeoLocation on local database
	 */
	@Override
	public int update(Object obj) {
		GeoLocation geo = (GeoLocation) obj;
		ContentValues values = new ContentValues();
		values.put(GeoLocation._ID, geo.getId());
		values.put(GeoLocation.PLACE_NAME, geo.getPlaceName());
		values.put(GeoLocation.LATITUDE, geo.getLatitude());
		values.put(GeoLocation.LONGITUDE, geo.getLongitude());
		values.put(GeoLocation.ADDRESS, geo.getAddress());
		values.put(GeoLocation.ID_GOOGLE_LOCATION, geo.getIdGoogleLocation());
		values.put(GeoLocation.ID_SUBTYPE_INFORMATION, geo.getIdSubtypeInformation());
		values.put(GeoLocation.PHONE_NUMBER, geo.getPhoneNumber());
		
		String _id = String.valueOf(geo.getId());
		String where = GeoLocation._ID + "=?";
		String[] whereArgs = new String[] { _id };
		return update(ctx, TABLE_NAME, values, where, whereArgs);
	}
	
	/**
	 * To get all GeoLocations from local database
	 * @return
	 */
	public List<GeoLocation> getAllMark(){
		try {
			Cursor c = getAllCursors();
			List<GeoLocation> geos = new ArrayList<GeoLocation>();
			if(c.moveToFirst()){
				int idxid = c.getColumnIndex(GeoLocation._ID);
				int idxplaceName = c.getColumnIndex(GeoLocation.PLACE_NAME);
				int idxlatitude = c.getColumnIndex(GeoLocation.LATITUDE);
				int idxlongitude = c.getColumnIndex(GeoLocation.LONGITUDE);
				int idxaddress = c.getColumnIndex(GeoLocation.ADDRESS);
				int idxidGoogleLocation = c.getColumnIndex(GeoLocation.ID_GOOGLE_LOCATION);
				int idxidSubtypeInformation = c.getColumnIndex(GeoLocation.ID_SUBTYPE_INFORMATION);
				int idxphoneNumber = c.getColumnIndex(GeoLocation.PHONE_NUMBER);
				do{
					GeoLocation geo = new GeoLocation(c.getLong(idxid), c.getString(idxplaceName), c.getDouble(idxlatitude), 
							c.getDouble(idxlongitude), c.getString(idxaddress), c.getString(idxidGoogleLocation), c.getLong(idxidSubtypeInformation),
							c.getString(idxphoneNumber));
					geos.add(geo);
				} while(c.moveToNext());
			}
			return geos;
		} catch(Exception e){
			Log.e("GeoLocation", "ERRO AO BUSCAR MARCACOES NO MAPA: " + e.toString());
			return null;
		}
	}
	
	/** 
	 * To get an cursor with all the GeoLocations
	 * @return Cursor */
	private Cursor getAllCursors(){
		try{
			return db.query(TABLE_NAME, GeoLocation.COLUMNS, null, null, null, null, null);
		}
		catch (SQLException e){
			Log.e("GeoLocation", "ERRO AO BUSCAR MARCACOES NO MAPA: " + e.toString());
			return null;
		}
	}

}

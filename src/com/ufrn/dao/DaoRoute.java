package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Route;

public class DaoRoute extends AbstractRepository {

	protected static final String TABLE_NAME = "Rotas";
	private Context ctx;
	
	public DaoRoute(Context ctx){
		this.ctx = ctx;
		db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
	}
	
	public DaoRoute(){}
	
	@Override
	public long save(Object obj){
		Route route = (Route) obj;
		long id = route.getId();
		if(id == 0)
			id = insert(route);
		else
			update(route);
		return id;
	}

	/**
	 * To insert an Route on local database
	 */
	@Override
	public long insert(Object obj) {
		Route route = (Route) obj;
		ContentValues values = new ContentValues();
		values.put(Route.DATE, route.getDate());
		return insert(ctx, TABLE_NAME, values);
	}

	/**
	 * To update an Route on local database
	 */
	@Override
	public int update(Object obj) {
		Route route = (Route) obj;
		ContentValues values = new ContentValues();
		values.put(Route.DATE, route.getDate());
		
		String _id = String.valueOf(route.getId());
		String where = Route._ID + "=?";
		String[] whereArgs = new String[] { _id };
		return update(ctx, TABLE_NAME, values, where, whereArgs);
	}
	
	/**
	 * To get all Route from local database
	 * @return
	 */
	public List<Route> getAllRoutes(){
		try {
			Cursor c = getAllCursors();
			List<Route> routes = new ArrayList<Route>();
			if(c.moveToFirst()){
				int idxid = c.getColumnIndex(Route._ID);
				int idxdate = c.getColumnIndex(Route.DATE);
				do{
					Route route = new Route(c.getLong(idxid), c.getString(idxdate));
					routes.add(route);
				} while(c.moveToNext());
			}
			return routes;
		} catch(Exception e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS: " + e.toString());
			return null;
		}
	}
	
	/** 
	 * To get an cursor with all the Route
	 * @return Cursor */
	private Cursor getAllCursors(){
		try{
			return db.query(TABLE_NAME, Route.COLUMNS, null, null, null, null, null);
		}
		catch (SQLException e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS: " + e.toString());
			return null;
		}
	}
	
	/**
	 * To get all Route by date from local database
	 * @return
	 */
	public Route findRouteByDate(String date){
		try {
			Cursor c = db.query(TABLE_NAME, Route.COLUMNS, Route.DATE + "='" + date + "'", null, null, null, null);
			if(c.getCount()>0){
				c.moveToFirst();
				int idxid = c.getColumnIndex(Route._ID);
				int idxdate = c.getColumnIndex(Route.DATE);
				Route route = new Route(c.getLong(idxid), c.getString(idxdate));
				return route;
			}
			return null;
		} catch(Exception e){
			Log.e(TABLE_NAME, "ERRO AO BUSCAR AS ROTAS: " + e.toString());
			return null;
		}
	}
	
}
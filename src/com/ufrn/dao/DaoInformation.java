package com.ufrn.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.ufrn.database.AbstractRepository;
import com.ufrn.domain.Information;


	public class DaoInformation extends AbstractRepository {
		
		protected static final String TABLE_NAME = "Tipo_Informacao";
		private Context ctx;
		
		public DaoInformation(Context ctx) {
			this.ctx = ctx;
			db = this.ctx.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		}
		
		@Override
		public long save(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long insert(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int update(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public List<Information> getAllTipoInformacao(){
			try {
				Cursor c = getAllCursors();
				List<Information> informacoes = new ArrayList<Information>();
				if(c.moveToFirst()){
					int idxcodigoTipo = c.getColumnIndex(Information._ID);
					int idxname = c.getColumnIndex(Information.NOME);
					
					do{
						Information info = new Information(c.getLong(idxcodigoTipo), c.getString(idxname));
						informacoes.add(info);
					} while(c.moveToNext());
				}
				return informacoes;
			} catch(Exception e){
				Log.e("TipoInformacao", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}

		private Cursor getAllCursors() {
			try{
				return db.query(TABLE_NAME, Information.COLUMNS, null, null, null, null, null);
			}
			catch (SQLException e){
				Log.e("TipoInforma��o", "ERRO AO BUSCAR MARCAÇÕES NO MAPA: " + e.toString());
				return null;
			}
		}
}
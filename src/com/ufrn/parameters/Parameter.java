package com.ufrn.parameters;

public class Parameter {

	public static int cod_language;
	
	public static int getCodLanguage() {
		return cod_language;
	}
	
	public static void setCodLanguage(int cod_language) {
		Parameter.cod_language = cod_language;
	}
	
}

package com.ufrn.cloudUtils;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.ufrn.domain.GeoLocation;

public class HttpRequest {

	private final static String GET = "GET";
	
	/**
	 * get an generic Object from server
	 * @param url
	 * @return
	 */
	public static List<GeoLocation> getGeoLocations(String url){
		//listaDadosGooglePlace no fim da url
		try {
			URL u = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod(GET);
			conn.setDoInput(true);
			conn.setDoOutput(false);
			conn.connect();
			InputStream in = conn.getInputStream();
			DataInputStream dataIn = new DataInputStream(in);
			int qtd = dataIn.readInt();
			List<GeoLocation> geos = new ArrayList<GeoLocation>();
			for(int i=0; i<qtd; ++i){
				GeoLocation geo = new GeoLocation();
				geo.deserialize(dataIn);
				geos.add(geo);
			}
			//Closing connections
			dataIn.close();
			in.close();
			return geos;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}

package com.ufrn.service;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ufrn.dao.DaoRoute;
import com.ufrn.dao.DaoRouteDetails;
import com.ufrn.domain.Route;
import com.ufrn.domain.RouteDetails;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

@SuppressLint("SimpleDateFormat")
public class LocationService extends Service {

	private final String TAG = "LocationService";
	
	private LocationManager locationManager;
	private LocationListener locationListener;	
	
	private DaoRouteDetails routeStorage;
	private DaoRoute daoRoute;

	@Override
	public void onCreate() {
		routeStorage = new DaoRouteDetails(this);
		daoRoute = new DaoRoute(this);
		initLocator();
		super.onCreate();
	}
		
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {		
		Log.d(TAG, "Started!");		
		boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		try{
		if(!enabled){
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 10f, locationListener);
			return START_STICKY;
		}
		else{
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 10f, locationListener);
			return START_STICKY;
		}
		}catch (Exception e) {
		 	e.printStackTrace();
		}
		return startId;
	}

			
	private void initLocator() {
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		locationListener = new LocationListener() {			
			@Override
			public void onLocationChanged(Location loc) {
				if(!routeStorage.latlngExist(loc.getLatitude(), loc.getLongitude())){
					Log.d(TAG, "Location changed: "+loc.getLongitude());
					// Create an instance of SimpleDateFormat used for formatting 
					// the string representation of date (month/day/year)
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					DateFormat hf = new SimpleDateFormat("hh:mm:ss");
	
					// Get the date today using Calendar object.
					Date today = Calendar.getInstance().getTime(); 
					Date today2 = new Date();
					
					// Using DateFormat format method we can create a string 
					// representation of a date with the defined format.
					String dateString = df.format(today);
					String HourString = hf.format(today2);
					Route route = daoRoute.findRouteByDate(dateString);
					long idRoute = 0;
					
					if(route != null)
						idRoute = route.getId();
					else {
						route = new Route(dateString);
						idRoute = daoRoute.save(route);
					}
	
					RouteDetails r = new RouteDetails(loc.getLatitude(), loc.getLongitude(), idRoute, HourString);
					routeStorage.save(r);
				}
			}

			@Override
			public void onProviderDisabled(String provider) { }

			@Override
			public void onProviderEnabled(String provider) { }

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) { }
		};			
	}
	
	@Override
	public IBinder onBind(Intent intent) { 
		return null; 
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "Destroyed!");
		super.onDestroy();
	}
}

package com.ufrn.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper{
	
	private static final String CATEGORY = "guia_turistico";
	private String[] scriptSQLCreate;
	private String scriptSQLDelete;
	
	public SQLiteHelper(Context ctx, String databaseName, int databaseVersion, String[] scriptSQLCreate, String scriptSQLDelete){
		super(ctx, databaseName, null, databaseVersion);
		this.scriptSQLCreate = scriptSQLCreate;
		this.scriptSQLDelete = scriptSQLDelete;
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(CATEGORY, "Criando banco com SQL");
		int qtdScripts = scriptSQLCreate.length;
		for(int i = 0; i < qtdScripts; ++i){
			String sql = scriptSQLCreate[i];
			Log.i(CATEGORY, sql);
			db.execSQL(sql);
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(CATEGORY, "Atualizando da versão " + oldVersion + " para " + newVersion + ". Todos os registros serão deletados.");
		Log.i(CATEGORY, scriptSQLDelete);
		db.execSQL(scriptSQLDelete);
		onCreate(db);
	}
}
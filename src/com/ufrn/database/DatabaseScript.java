package com.ufrn.database;

import android.content.Context;

public class DatabaseScript extends Database {

	private static final String SCRIPT_DATABASE_DELETE = "DROP TABLE IF EXISTS ordem_servico; DROP TABLE IF EXISTS produto";
	private static final String[] SCRIPT_DATABASE_CREATE = new String[]{
		                                           
		
		
	//	"CREATE TABLE IF NOT EXISTS Origem_Dados (codigo_origem INTEGER PRIMARY_KEY, nome_fonte VARCHAR(250) NOT NULL);",
		"CREATE TABLE IF NOT EXISTS Informacoes_Posicao_Geograficas (codigo_informacao INTEGER PRIMARY KEY, place_name VARCHAR(250) NOT NULL, latitude DOUBLE NOT NULL, longitude DOUBLE NOT NULL, vicinity VARCHAR(250) NOT NULL, id_google_location VARCHAR(50) NOT NULL, FOREIGN KEY (codigo_informacao) REFERENCES Informacao_Turistica(codigo_informacao));",
		"CREATE TABLE IF NOT EXISTS Rotas (codigo_rota INTEGER PRIMARY KEY, data VARCHAR(50) NULL);",
		"CREATE TABLE IF NOT EXISTS Rotas_Detalhes (codigo_rota_detalhe INTEGER PRIMARY KEY, latitude DOUBLE NOT NULL, longitude DOUBLE NOT NULL, codigo_rota INTEGER NOT NULL, hora VARCHAR(50) NULL, FOREIGN KEY (codigo_rota) REFERENCES Rotas(codigo_rota));",
		"CREATE TABLE IF NOT EXISTS Perfil (codigo_perfil INTEGER PRIMARY KEY);",
		"CREATE TABLE IF NOT EXISTS Lingua (codigo_lingua INTEGER PRIMARY KEY, nome VARCHAR(250) NOT NULL);",
		//"CREATE TABLE IF NOT EXISTS Subtipo_Informacao_Detalhes (codigo_tipo INTEGER PRIMARY KEY, codigo_subtipo INTEGER NOT NULL, codigo_lingua INTEGER NOT NULL, nome VARCHAR(250) NOT NULL, FOREIGN KEY (codigo_tipo) REFERENCES Subtipo_Informacao(codigo_tipo), FOREIGN KEY (codigo_subtipo) REFERENCES Subtipo_Informacao(codigo_subtipo), FOREIGN KEY (codigo_lingua) REFERENCES Lingua(codigo_lingua));",		
		"CREATE TABLE IF NOT EXISTS Perfil_Detalhes (codigo_perfil INTEGER NOT NULL, codigo_lingua INTEGER NOT NULL, nome VARCHAR(250) NOT NULL, FOREIGN KEY (codigo_perfil) REFERENCES Lingua(codigo_lingua), FOREIGN KEY (codigo_lingua) REFERENCES Perfil(codigo_perfil));",
		"CREATE TABLE IF NOT EXISTS Perfil_Informacao (codigo_informacao INTEGER PRIMARY KEY, codigo_perfil INTEGER NOT NULL, FOREIGN KEY (codigo_informacao) REFERENCES Informacao_Turistica(codigo_informacao), FOREIGN KEY (codigo_perfil) REFERENCES Perfil(codigo_perfil));",
		"CREATE TABLE IF NOT EXISTS Roteiro (codigo_roteiro INTEGER PRIMARY KEY, tipo VARCHAR(250) NOT NULL);",
		"CREATE TABLE IF NOT EXISTS Roteiro_Detalhe (codigo_roteiro INTEGER NOT NULL, codigo_lingua INTEGER NOT NULL, nome VARCHAR(250) NOT NULL, FOREIGN KEY (codigo_roteiro) REFERENCES Roteiro(codigo_roteiro), FOREIGN KEY (codigo_lingua) REFERENCES Roteiro(codigo_roteiro));",
		"CREATE TABLE IF NOT EXISTS Item_Roteiro (codigo_roteiro INTEGER NOT NULL, codigo_informacao INTEGER NOT NULL, seg_realizacao INTEGER NOT NULL, FOREIGN KEY (codigo_roteiro) REFERENCES Roteiro(codigo_roteiro), FOREIGN KEY (codigo_informacao) REFERENCES Informacao_Turistica(codigo_informacao));",
		
		//"create table if not exists DadosGerais(id INTEGER PRIMARY KEY, codigo_informacao INTEGER, nome_fonte VARCHAR(250), tipo_informacao VARCHAR(250), subtipo_informacao VARCHAR(250), lingua VARCHAR(50), detalhe_turista VARCHAR(250), descricao VARCHAR(250));",

		"CREATE TABLE IF NOT EXISTS Tipo_Informacao (codigo_tipo INTEGER PRIMARY KEY, nome VARCHAR(250) NOT NULL);",
		"CREATE TABLE IF NOT EXISTS Subtipo_Informacao (codigo_subtipo INTEGER PRIMARY KEY, codigo_tipo INTEGER NOT NULL, nome_subtipo VARCHAR(250) NOT NULL, FOREIGN KEY (codigo_tipo) REFERENCES Tipo_Informacao(codigo_tipo));",
		"CREATE TABLE IF NOT EXISTS Informacao_Turistica (codigo_informacao INTEGER PRIMARY KEY, codigo_tipo INTEGER NOT NULL, codigo_subtipo INTEGER NOT NULL, nome_info VARCHAR(250), mini_descricao VARCHAR(300), FOREIGN KEY (codigo_subtipo) REFERENCES Subtipo_Informacao(codigo_subtipo), FOREIGN KEY (codigo_tipo) REFERENCES Tipo_Informacao(codigo_tipo));",
		"CREATE TABLE IF NOT EXISTS Informacao_Turistica_Detalhes (codigo_detalhes INTEGER PRIMARY KEY, codigo_informacao INTEGER NOT NULL, codigo_lingua INTEGER NOT NULL, nome VARCHAR(250) NOT NULL, descricao VARCHAR(1500), endereco VARCHAR(250) NOT NULL, FOREIGN KEY (codigo_informacao) REFERENCES Informacao_Turistica(codigo_informacao), FOREIGN KEY (codigo_lingua) REFERENCES Lingua(codigo_lingua));",
		
		//INSERINDO DADOS EM TIPO INFORMA��O
		"INSERT INTO Tipo_Informacao (codigo_tipo, nome) VALUES (1,'O que fazer');",
		"INSERT INTO Tipo_Informacao (codigo_tipo, nome) VALUES (2,'Comer e beber');",
		"INSERT INTO Tipo_Informacao (codigo_tipo, nome) VALUES (3,'Onde ficar');",
		"INSERT INTO Tipo_Informacao (codigo_tipo, nome) VALUES (4,'Servi�os');",
		
		//INSERINDO DADOS EM SUBTIPO INFORMA��O
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (1,1, 'museuCâmaraCascudo');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (2,1, 'museus2');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (3,2, 'Restaurantes');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (4,2, 'Bares');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (5,3, 'Hoteis');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (6,3, 'Pousadas');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (7,3, 'Albergues');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (8,4, 'Igreja');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (9,4, 'Santuario');",
		"INSERT INTO Subtipo_Informacao (codigo_subtipo, codigo_tipo, nome_subtipo) VALUES (10,4, 'Capela');",

		//INSERINDO DADOS EM INFORMACAO TURISTICA
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (1,1,1, 'museu1 abc', 'mini-drescricao');",
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (2,1,2, 'museu2', 'mini-drescricao');",
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (3,2,3, 'atra��o1', 'mini-drescricao');",
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (4,2,1, 'atra��o2', 'mini-drescricao');",
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (5,3,2, 'restaurante1', 'mini-drescricao');",
		"INSERT INTO Informacao_Turistica (codigo_informacao, codigo_tipo, codigo_subtipo, nome_info, mini_descricao) VALUES (6,4,3, 'restaurante2', 'mini-drescricao');",
		
		//INSERINDO DADOS EM INFORMACAO TURISTICA DETALHES
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (1,1,1,'Bobs','Museu Camara Cascudoojodowiouhoefhféhofihoeifhoeihofiehofhoehfiehofiheof" +
		"fjpeojfpoejpofpejfpoejpfojepof','Avenida Hermes da Fonseca, 1398 - TIROL');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (2,2,2,'Bobs','Museu Camara Museum','Avenida Hermes da Fonseca, 1398 - TIROL');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (3,3,1,'Bobs','Igreja Nossa Senhora da Apresentação','Praça André de Albuquerque, s/n - Cidade Alta');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (4,4,2,'Bobs','Nossa Senhora da Apresentação Old Cathedral','Igreja Cat�lica Nossa Senhora da Apresentação');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (5,5,1,'Bobs','Fortaleza dos Reis Magos','Natal - RN');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (6,6,2,'Bobs','Kings Fortress','Natal - RN');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (7,7,1,'Bobs','Camarões','Rua Pedro Fonseca Filho, 8887 - Ponta Negra');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (8,8,2,'Bobs','Camarões','Rua Pedro Fonseca Filho, 8887 - Ponta Negra');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (9,9,1,'Bobs','Frans Café','Avenida Praia de Ponta Negra, 8888 - Ponta Negra');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (10,10,2,'Bobs','Frans Café','Avenida Praia de Ponta Negra, 8888 - Ponta Negra');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (11,11,1,'Bobs','Bobs','Bobs');",
		"INSERT INTO Informacao_Turistica_Detalhes (codigo_detalhes, codigo_informacao, codigo_lingua, nome, descricao, endereco) VALUES (12,12,2,'Bobs','Bobs','Bobs');",
			
				
		
		
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (1,5,1);",
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (1,7,2);",
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (1,9,3);",
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (2,9,1);",
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (2,10,2);",
				"INSERT INTO Item_Roteiro (codigo_roteiro, codigo_informacao, seg_realizacao) VALUES (2,11,2);",
				
				"INSERT INTO Lingua (codigo_lingua, nome) VALUES (1, 'Portugues');",
				"INSERT INTO Lingua (codigo_lingua, nome) VALUES (2, 'English');",
				
//				"INSERT INTO Origem_Dados (codigo_origem, nome_fonte) VALUES (1,'Manual');",
//				"INSERT INTO Origem_Dados (codigo_origem, nome_fonte) VALUES (2,'GooglePlace');",
//				"INSERT INTO Origem_Dados (codigo_origem, nome_fonte) VALUES (3,'FaceBook');",

				"INSERT INTO Perfil (codigo_perfil) VALUES (1);",
				"INSERT INTO Perfil (codigo_perfil) VALUES (2);",

				"INSERT INTO Perfil_Detalhes (codigo_perfil, codigo_lingua, nome) VALUES (1,1,'Solteiro');",
				"INSERT INTO Perfil_Detalhes (codigo_perfil, codigo_lingua, nome) VALUES (1,1,'Single');",
				"INSERT INTO Perfil_Detalhes (codigo_perfil, codigo_lingua, nome) VALUES (2,1,'Casado');",
				"INSERT INTO Perfil_Detalhes (codigo_perfil, codigo_lingua, nome) VALUES (2,1,'Married');",
				
				//+_+_+_+_+_+_+_+_+_+_+ PERFIL INFORMACAO +_+_+_+_+_+_+
				
				"INSERT INTO Roteiro (codigo_roteiro, tipo) VALUES (1,1);",
				"INSERT INTO Roteiro (codigo_roteiro, tipo) VALUES (2,1);",

				"INSERT INTO Roteiro_Detalhe (codigo_roteiro, codigo_lingua, nome) VALUES (1,1,'Cultural');",
				"INSERT INTO Roteiro_Detalhe (codigo_roteiro, codigo_lingua, nome) VALUES (1,2,'Cultural');",
				"INSERT INTO Roteiro_Detalhe (codigo_roteiro, codigo_lingua, nome) VALUES (2,1,'Compras');",
				"INSERT INTO Roteiro_Detalhe (codigo_roteiro, codigo_lingua, nome) VALUES (2,2,'Shopping');",
			
				};

		
	private SQLiteHelper dbHelper;
	
	public DatabaseScript(Context ctx){
		dbHelper = new SQLiteHelper(ctx, DatabaseScript.DATABASE_NAME, DatabaseScript.DATABASE_VERSION, DatabaseScript.SCRIPT_DATABASE_CREATE, DatabaseScript.SCRIPT_DATABASE_DELETE);
		db = dbHelper.getWritableDatabase();
	}
	@Override
	public void close(){
		super.close();
		if(dbHelper != null){
			dbHelper.close();
		}
	}
}
package com.ufrn.database;

import android.content.ContentValues;
import android.content.Context;

public abstract class AbstractRepository extends Database implements InterfaceRepository{

	@Override
	public abstract long save(Object obj);

	@Override
	public abstract long insert(Object obj);

	@Override
	public abstract int update(Object obj);
	
	@Override
	public String delete(long id){
		return null;
	}

	@Override
	public long insert(Context ctx, String nome_tabela, ContentValues values){
		long id = db.insert(nome_tabela, null, values);
		return id;
	}
	
	@Override
	public int update(Context ctx, String nome_tabela, ContentValues values, String where, String[] whereArgs){
		int count = db.update(nome_tabela, values, where, whereArgs);
		return count;
	}	
}
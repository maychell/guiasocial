package com.ufrn.database;

import android.database.sqlite.SQLiteDatabase;

public class Database {

	protected static final String DATABASE_NAME = "guia_mobile";
	protected static final int DATABASE_VERSION = 1;
	protected SQLiteDatabase db;
	
	public void close(){
		if(db != null){
			db.close();
		}
	}
}

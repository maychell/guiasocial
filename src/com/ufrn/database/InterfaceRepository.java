package com.ufrn.database;

import android.content.ContentValues;
import android.content.Context;


public interface InterfaceRepository {

	public long save(Object obj);
	public long insert(Object obj);
	public int update(Object obj);
	public long insert(Context ctx, String nome_tabela, ContentValues values);
	public int update(Context ctx, String nome_tabela, ContentValues values, String where, String[] whereArgs);
	public String delete(long id);
	public void close();
	
}
